﻿
using System;

namespace Optimizer_Exceptions
{
    public class OptimizerException : Exception
    {
        public enum OptimizerExceptionType
        {
            InputFileNotFound,
            ColumnNotFound,
            UnknownError,
            ExecutionStoppedOnUserRequest,
            ConfigurationError,
            MissingData
        }

        private String [] details;
        private OptimizerExceptionType code;

        public OptimizerException(OptimizerExceptionType generalReason, String detailedExplanation)
        {
            details = new string[1];
            details[0] = detailedExplanation;
            code = generalReason;
        }

        public OptimizerException(OptimizerExceptionType generalReason, String [] detailedExplanation)
        {
            details = detailedExplanation;
            code = generalReason;
        }

        public String [] Details
        {
            get { return details; }
        }

        public OptimizerExceptionType GeneralReason
        {
            get { return code; }
        }

        
    }

    
}
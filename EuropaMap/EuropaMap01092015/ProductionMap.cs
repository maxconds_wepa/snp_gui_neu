﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EuropaMap
{

    /* BefehlszeilenArgumente: 
     * 1.Argument
     * Plantlocations.csv 
     * 
     * 2.
     * optimierung_output_graphics.csv 
     * 
     * 3.
     * optimierung_output_graphics_transport.csv 
     * 
     * 4.Bild
     * europa.png 
     * 
     * 5. Demo = 0, 1 = real Setzt den Initialen Skalierungsfaktor
     * 1
     * 
     * BEISPIELE: 
     * EUROPA
     * Plantlocations.csv optimierung_output_graphics.csv optimierung_output_graphics_transport.csv europa.png 1
     * 
     * DEutschland
     * PlantlocationsDemo.csv output_graphic_demo.csv optimierung_output_graphics_transport.csv deutschland.png 0
     * 
     */


    public partial class ProductionMap : Form
    {

        List<TextBox> standortTextBoxList = new List<TextBox>();



        private String imagefile;
        private String dataFile;
        private String transportDataFile;
        private String planlocationsFile;

        private Char separator = ';';


        List<String> selectedZeitraumItems = new List<String>();




        string curProdItem = "";

        List<String> zeitraumList = new List<String>();//Enthält die 3 verschiedenen Monate + gesamt


        List<List<String>> dataList = new List<List<String>>(); //in dieser Liste werden die Standort Daten abgespeichrt, welche aus der Datei des ersten Befehlszeilenargument gelesen wird
        List<List<String>> transportDataList = new List<List<String>>();//in dieser Liste werden die Transport Daten abgespeichrt, welche aus der Datei des zweiten Befehlszeilenargument gelesen wird
        List<List<String>> planlocationsList = new List<List<string>>();//Rohdaten als Liste der Standorte
        List<String> kopfzeileIC = new List<String>();





        List<string> productionList = new List<string>();//Enthält alle auszuwählende Attribute


        Bitmap image;


        List<StandOrt> standOrtListe = new List<StandOrt>();//enthält alle Standorte 


        double scale;//Skalierungs faktor des Bildes
        int imageWidth;
        int imageHeight;

        /*Konstruktor
         * 1.	 PlantLocations.csv   ( Werke mit zugehöriger Pixel , Name, Postleitzahl )
           2.	optimierung_output_graphics.csv (Auf Werk bezogene Daten  z.B. Produktion in Paletten)
           3.	optimierung_output_graphics_ic.csv (Inter-Werkbeziehungen  z.B.  ausgehende IC-Transporte in Paletten )
           4.	Bilddatei (z.B. europa.png)
           5.	o/1 für Demo /Real (setzt den initialen Skalierungsfaktor)
         * 
         */
        public ProductionMap(String planlocationsFile, String dataFile, String transportDataFile, String imageFile, String version)
        {
            this.planlocationsFile = planlocationsFile;
            this.dataFile = dataFile;
            this.transportDataFile = transportDataFile;
            this.imagefile = imageFile;
            image = new Bitmap(imagefile);

            imageWidth = image.Width;//2384;//1081 klein
            imageHeight = image.Height;//1863;//893 klein
            //  imagefile = "europa.png";
            if (version.Equals("0"))
            {
                scale = 0.22;
            }

            else
            {
                scale = 0.4;
            }




            InitializeComponent();

        }

        public void readStandortliste()
        {
            standOrtListe = new List<StandOrt>();//enthält alle Standorte 


            for (int i = 0; i < planlocationsList.Count(); i++)
            {
                Point p = new Point(Convert.ToInt32(planlocationsList[i][2]), Convert.ToInt32(planlocationsList[i][3]));
                StandOrt ort = new StandOrt(planlocationsList[i][0], planlocationsList[i][1], p);
                standOrtListe.Add(ort);
            }

        }

        //Initialisierung der Komponenten ListBox und CheckBoxen(Laden der CSV Dateien)
        private void ProductionMap_Load(object sender, System.EventArgs e)
        {


            List<String> zeitraumListKomplett = new List<String>();


            productionList = new List<string>();
            dataList = new List<List<String>>();
            transportDataList = new List<List<String>>();
            bool headline = true;
            Data.read_header(ref dataFile, ref separator, ref productionList);
            productionList.RemoveAt(0);
            productionList.RemoveAt(0);
            Data.read_header(ref transportDataFile, ref separator, ref kopfzeileIC);
            zeitraumList = new List<String>();


            Data.read_data(ref dataFile, ref separator, ref headline, ref dataList);
            Data.read_data(ref transportDataFile, ref separator, ref headline, ref transportDataList);
            headline = true;
            Data.read_data(ref planlocationsFile, ref separator, ref headline, ref planlocationsList);
            readStandortliste();

            for (int i = 0; i < dataList.Count(); i++)
            {
                zeitraumListKomplett.Add(dataList[i][1]);
            }


            for (int i = 0; i < zeitraumListKomplett.Count(); i++)
            {
                if (!zeitraumList.Contains(zeitraumListKomplett.ElementAt(i)))
                {
                    zeitraumList.Add(zeitraumListKomplett.ElementAt(i));
                }

            }





            CheckBoxMonat1.Checked = true;
            Produktion.DrawMode = DrawMode.OwnerDrawFixed;
            Produktion.DrawItem += new DrawItemEventHandler(Produktion_DrawItem);
            Zeitraum.DataSource = zeitraumList;
            CheckBoxMonat1.Text = zeitraumList[0];
            checkBoxMonat2.Text = zeitraumList[1];
            checkBoxMonat3.Text = zeitraumList[2];

            Produktion.DataSource = productionList;

            image = new Bitmap(imagefile);

            //Punkte an Skalierung anpassen
            for (int i = 0; i < standOrtListe.Count(); i++)
            {
                standOrtListe[i].scale(scale);
            }


            image = new Bitmap(image, new Size((int)(imageWidth * scale), (int)(imageHeight * scale)));



            pictureBox1.Image = (Image)image;


            // Connect the Paint event of the PictureBox to the event handler method.
            pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.image_Paint);
            pictureBox1.MouseClick += new MouseEventHandler(Control1_MouseClick);
            pictureBox1.MouseMove += new MouseEventHandler(Control1_MouseMove);
            // Add the PictureBox control to the Form.
            Controls.Add(pictureBox1);
        }


        //Wird verwendet um die ListBox in alternating Colors darzustellen
        private void Produktion_DrawItem(object sender, DrawItemEventArgs e)
        {

            bool isSelected = ((e.State & DrawItemState.Selected) == DrawItemState.Selected);

            e.DrawBackground();
            Graphics g = e.Graphics;
            Font drawFont = new Font("Arial", 10);
            Color color = isSelected ? SystemColors.Highlight :
            e.Index % 2 == 0 ? Color.WhiteSmoke : Color.White;

            SolidBrush drawBrush = new SolidBrush(Color.Black);




            g.FillRectangle(new SolidBrush(color), e.Bounds);


            e.Graphics.DrawString(productionList.ElementAt(e.Index), drawFont, drawBrush, e.Bounds);
            // Print text

            e.DrawFocusRectangle();
        }


        /*
         *Zeichnet die einzelnen Elemnente der Standorte in der Reinfolge der Aufrufe 
         *ein repaint tritt ein durch den Aufruf von Invalidate();
         */
        private void image_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {



            Graphics graphics = e.Graphics;
            graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;



            foreach (StandOrt ort in standOrtListe)
            {
                ort.paintVerbindungen(e);
            }



            for (int i = 0; i < standOrtListe.Count(); i++)
            {

                standOrtListe[i].paintEllipse(e);

            }




            for (int i = 0; i < standOrtListe.Count(); i++)
            {
                standOrtListe[i].paintName(e);
            }



            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelectedDetail() == true)
                {
                    ort.paintDetailInformation(e, curProdItem);
                }
            }
        }




        /*
         * Lädt die Daten der Standorte  
         */
        private void loadStandortData()
        {
            //Liste Leeren
            foreach (StandOrt ort in standOrtListe)
            {
                ort.setValue(0);
            }
            StandOrt.valueAll = 0;




            //Addiert weitere ausgewählte Item auf
            for (int j = 0; j < selectedZeitraumItems.Count; j++)
            {
                String curZeitraumItem = selectedZeitraumItems[j];
                int indexZeitraum = zeitraumList.IndexOf(curZeitraumItem);
                int indexProduction = productionList.IndexOf(curProdItem) + 2;//Werk und Monat sind in der auswahl nicht vorhanden in den Daten aber schon  

                List<List<String>> selectedDateDataList = new List<List<String>>();
                if (indexZeitraum >= 0)
                {


                    for (int i = 0; i < dataList.Count(); i++)
                    {

                        if (dataList[i][1] == curZeitraumItem)
                        {
                            selectedDateDataList.Add(dataList[i]);
                        }

                    }

                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {
                        for (int k = 0; k < selectedDateDataList.Count; k++)
                        {
                            if (standOrtListe[i].plz == selectedDateDataList[k][0])
                            {
                                standOrtListe[i].addValue(Convert.ToDouble(selectedDateDataList[k][indexProduction].Replace(".", ",")));
                            }
                        }
                    }


                    double sum = 0;

                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {
                        sum += standOrtListe[i].getValue();


                    }
                    StandOrt.valueAll = sum;
                    for (int i = 0; i < standOrtListe.Count(); i++)
                    {

                        standOrtListe[i].calculatePercentage(StandOrt.valueAll);
                    }

                }
            }

        }


        /*
         *Lädt die daten aus der Transportdatendatei  
         */
        private void loadTransportData()
        {

            //null setzten
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelected() == true)
                {
                    StandOrt.valueAll = ort.getValue();
                    ort.calculatePercentage(ort.getValue());
                    foreach (StandOrt neighbour in ort.getNeighbours())
                    {
                        neighbour.setValue(0);
                        neighbour.calculatePercentage(ort.getValue());
                    }
                }
            }



            //einlesen
            for (int j = 0; j < selectedZeitraumItems.Count; j++)
            {

                String curZeitraumItem = selectedZeitraumItems[j];


                List<List<String>> selectedDateTransportDataList = new List<List<String>>();



                //aus der Gesamten Datei werden alle einträge mit falschem Datum nichtübernommen
                for (int i = 0; i < transportDataList.Count(); i++)
                {

                    if (transportDataList[i][2] == curZeitraumItem)
                    {

                        selectedDateTransportDataList.Add(transportDataList[i]);
                    }


                }
                int indexZeitraum = zeitraumList.IndexOf(curZeitraumItem);


                int indexProduction = kopfzeileIC.IndexOf(curProdItem);//productionList.IndexOf(curProdItem) + 3;//WerkA WerkB und Monat sind in der auswahl nicht vorhanden in den Daten aber schon  

                if (indexProduction >= 0)
                {
                    //nachbarwerte Setzten
                    for (int k = 0; k < selectedDateTransportDataList.Count; k++)
                    {
                        foreach (StandOrt ort in standOrtListe)
                        {
                            if (ort.plz == selectedDateTransportDataList[k][0])
                            {

                                if (ort.getSelected() == true)
                                {

                                    ;
                                    foreach (StandOrt neighbour in ort.getNeighbours())
                                    {
                                        if (neighbour.plz == selectedDateTransportDataList[k][1])
                                        {
                                            neighbour.addValue(Convert.ToDouble(selectedDateTransportDataList[k][indexProduction].Replace(".", ",")));
                                            neighbour.calculatePercentage(ort.getValue());

                                        }
                                    }

                                }

                            }
                        }
                    }

                }
            }






        }


        /*falls eine neue Stadt, ein neues ProduktionsArgument oder eine anderer Zeitraum ausgewählt wurde wird selection Changed aufgerufen um die 
        * hier wird die Liste StandortLIste an die neue auswahl angepasst indem die LoadFunktionen aufgerufen werden 
         * und anschliesen wird das bild neu gezeichnet
        */
        private void selectionChanged()
        {


            reCalculateVerbindungen();
            selectedZeitraumItems.Clear();

            if (CheckBoxMonat1.Checked == true)
            {
                selectedZeitraumItems.Add(zeitraumList[0]);

            }

            if (checkBoxMonat2.Checked == true)
            {

                selectedZeitraumItems.Add(zeitraumList[1]);
            }
            if (checkBoxMonat3.Checked == true)
            {
                selectedZeitraumItems.Add(zeitraumList[2]);
            }

            if (Produktion.SelectedItem != null)
            {
                curProdItem = Produktion.SelectedItem.ToString();
            }


            if (selectedZeitraumItems.Count != 0 && curProdItem != "")
            {

                loadStandortData();
                loadTransportData();
                // Force the form to be redrawn with the image.
                pictureBox1.Invalidate();
                ;

            }

        }


        //wenn eine Stadt ausgewählt wird werden hier die alten Nachbarn gelöscht und die neuen erstellt
        private void reCalculateVerbindungen()
        {
            foreach (StandOrt ort in standOrtListe)
            {
                // ort.clearVerbindungen();
                ort.clearNeighbours();
                ort.isNeighbour = false;
            }


            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelected() == true)
                {


                    foreach (StandOrt ort2 in standOrtListe)
                    {
                        if (ort != ort2)// && ort2.getSelected() == true)
                        {
                            //ort.addVerbindung(ort2,1);
                            ort.addNeighbours(ort2);
                            ort2.isNeighbour = true;
                        }
                    }

                }
            }
        }



        private void Zeitraum_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectionChanged();
        }

        private void Produktion_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bei wahl eines neuen productionItem sollen alle Orte unselected werden 
            foreach (StandOrt ort in standOrtListe)
            {
                ort.setSelected(false);
            }
            selectionChanged();

        }


        //EVENT MOUSE CLICK
        private void Control1_MouseClick(Object sender, MouseEventArgs e)
        {
            bool einOrtIstSchonAusgewaehlt = false;
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.getSelected() == true)
                {
                    einOrtIstSchonAusgewaehlt = true;
                }
            }

            if (einOrtIstSchonAusgewaehlt == false)
            {

                bool ausgewaehlt = false;
                for (int i = 0; i < standOrtListe.Count; i++)
                {
                    if (standOrtListe[i].isMouseOver(e.X, e.Y))
                    {
                        if (standOrtListe[i].getSelected() == true)//Wenn Ort Schon ausgewählt war 
                        {
                            standOrtListe[i].setSelected(false);

                        }
                        else
                        {
                            if (kopfzeileIC.IndexOf(curProdItem) >= 0)
                            {
                                standOrtListe[i].setSelected(true);
                            }
                        }

                        ausgewaehlt = true;
                    };
                }
                if (ausgewaehlt == false)
                {
                    for (int i = 0; i < standOrtListe.Count; i++)
                    {
                        standOrtListe[i].setSelected(false);
                    }
                }
            }
            else
            {
                foreach (StandOrt ort in standOrtListe)
                {
                    ort.setSelected(false);
                }

            }
            reCalculateVerbindungen();
            selectionChanged();
            pictureBox1.Invalidate();


        }


        // EVENT MouseOver
        private void Control1_MouseMove(Object sender, MouseEventArgs e)
        {
            foreach (StandOrt ort in standOrtListe)
            {
                if (ort.isMouseOver(e.X, e.Y))
                {
                    //   pictureBox1.Controls.Add(ort.getStandortTextBox(curProdItem));


                    if (ort.getSelectedDetail() == false)
                    {
                        ort.setDetailSelected(true);
                        pictureBox1.Invalidate();
                    }

                }
                else
                {

                    if (ort.getSelectedDetail() == true)
                    {
                        ort.setDetailSelected(false);
                        pictureBox1.Invalidate();
                    }
                }
            }


        }


        //CHECKBOX lösen selectionhanged aus --> Die daten werden neu geladen
        /*   private void checkBoxGesamt_CheckedChanged(object sender, EventArgs e)
           {
               if (checkBoxGesamt.Checked == true)
               {

                   CheckBoxMonat1.Checked = true;
                   checkBoxMonat3.Checked = true;
                   checkBoxMonat2.Checked = true;

               }
               else
               {

                   CheckBoxMonat1.Checked = true;
                   checkBoxMonat3.Checked = false;
                   checkBoxMonat2.Checked = false;

               }
               selectionChanged();
           }
           */
        private void checkBoxMonat2_CheckedChanged(object sender, EventArgs e)
        {

            selectionChanged();
        }

        private void checkBoxMonat3_CheckedChanged(object sender, EventArgs e)
        {

            selectionChanged();
            selectionChanged();

        }

        private void CheckBoxMonat1_CheckedChanged(object sender, EventArgs e)
        {

            selectionChanged();

            selectionChanged();

        }

        private void scaleGraphic(double scale)
        {



            readStandortliste();

            loadStandortData();
            //Punkte an Skalierung anpassen
            for (int i = 0; i < standOrtListe.Count(); i++)
            {
                standOrtListe[i].scale(scale);
            }

            image = new Bitmap(imagefile);
            image = new Bitmap(image, new Size((int)(imageWidth * scale), (int)(imageHeight * scale)));



            pictureBox1.Image = (Image)image;
            pictureBox1.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (scale - 0.05 > 0.05)
            {
                scale = scale - 0.05;
                scaleGraphic(scale);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            scale = scale + 0.05;
            scaleGraphic(scale);
        }












    }
}

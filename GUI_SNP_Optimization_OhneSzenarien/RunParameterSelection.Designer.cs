﻿namespace GUI_SNP_Optimization
{
    partial class RunParameterSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnLoadSettingsFromOptimizationRun = new System.Windows.Forms.Button();
            this.dGVOptimizationRunConfigs = new System.Windows.Forms.DataGridView();
            this.btnBackWithoutAcceptance = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dGVOptimizationRunConfigs)).BeginInit();
            this.SuspendLayout();
            // 
            // btnLoadSettingsFromOptimizationRun
            // 
            this.btnLoadSettingsFromOptimizationRun.Location = new System.Drawing.Point(302, 176);
            this.btnLoadSettingsFromOptimizationRun.Name = "btnLoadSettingsFromOptimizationRun";
            this.btnLoadSettingsFromOptimizationRun.Size = new System.Drawing.Size(230, 23);
            this.btnLoadSettingsFromOptimizationRun.TabIndex = 48;
            this.btnLoadSettingsFromOptimizationRun.Text = "Übernehmen";
            this.btnLoadSettingsFromOptimizationRun.UseVisualStyleBackColor = true;
            this.btnLoadSettingsFromOptimizationRun.Click += new System.EventHandler(this.btnLoadSettingsFromOptimizationRun_Click);
            // 
            // dGVOptimizationRunConfigs
            // 
            this.dGVOptimizationRunConfigs.AllowUserToAddRows = false;
            this.dGVOptimizationRunConfigs.AllowUserToDeleteRows = false;
            this.dGVOptimizationRunConfigs.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dGVOptimizationRunConfigs.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dGVOptimizationRunConfigs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVOptimizationRunConfigs.Location = new System.Drawing.Point(12, 12);
            this.dGVOptimizationRunConfigs.MultiSelect = false;
            this.dGVOptimizationRunConfigs.Name = "dGVOptimizationRunConfigs";
            this.dGVOptimizationRunConfigs.ReadOnly = true;
            this.dGVOptimizationRunConfigs.Size = new System.Drawing.Size(1062, 158);
            this.dGVOptimizationRunConfigs.TabIndex = 46;
            // 
            // btnBackWithoutAcceptance
            // 
            this.btnBackWithoutAcceptance.Location = new System.Drawing.Point(553, 176);
            this.btnBackWithoutAcceptance.Name = "btnBackWithoutAcceptance";
            this.btnBackWithoutAcceptance.Size = new System.Drawing.Size(230, 23);
            this.btnBackWithoutAcceptance.TabIndex = 49;
            this.btnBackWithoutAcceptance.Text = "Ohne Übernahme zurück";
            this.btnBackWithoutAcceptance.UseVisualStyleBackColor = true;
            this.btnBackWithoutAcceptance.Click += new System.EventHandler(this.btnBackWithoutAcceptance_Click);
            // 
            // RunParameterSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1089, 205);
            this.Controls.Add(this.btnBackWithoutAcceptance);
            this.Controls.Add(this.dGVOptimizationRunConfigs);
            this.Controls.Add(this.btnLoadSettingsFromOptimizationRun);
            this.Name = "RunParameterSelection";
            this.Text = "Parameter aus vergangenen Optimierungsläufen";
            this.Load += new System.EventHandler(this.RunParameterSelection_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dGVOptimizationRunConfigs)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnLoadSettingsFromOptimizationRun;
        private System.Windows.Forms.DataGridView dGVOptimizationRunConfigs;
        private System.Windows.Forms.Button btnBackWithoutAcceptance;
    }
}
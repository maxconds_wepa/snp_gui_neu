﻿
Namespace UserMessages

    Public Enum MessageId
        INFO_MSG_OPTIMIZERSTARTED
        INFO_MSG_PREPAREDEMANDDATA
        INFO_MSG_CLEAROUTPUTTABLES
        INFO_MSG_GETDATA
        INFO_MSG_RUNREMOTEOPTIMIZATION
        INFO_MSG_WRITEDB
        INFO_MSG_GENERATE_DETAILED_OUTPUT_DATA
        INFO_MSG_WRITEDB_DETAILED_OUTPUT_DATA
        INFO_MSG_WRITEPAPERDEMANDS
        INFO_MSG_QLIKVIEWRELOAD
        INFO_MSG_DONE
        INFO_MSG_OPTIMIZATION_SESSION_COMPLETED
        ERROR_MSG_ARTICLES_WITHLACKING_INFORMATION_FOUND
        ERROR_MSG_WAREHOUSE_WITHLACKING_CAPACITY
        ERROR_MSG_QVS_TASK_EXECUTION_FAILED
        ERROR_MSG_QVS_TASK_EXECUTION_ABORTED
        ERROR_MSG_QVS_TASK_EXECUTION_UNKNOWN_REASON
        ERROR_MSG_QVS_TASK_EXECUTION_OTHER_ERROR
        ERROR_MSG_QVS_TASK_NOT_FOUND
        ERROR_MSG_CMD_LINE_WRONG_NUMBER_OF_PARAMETERS
        ERROR_MSG_CMD_LINE_NO_IMG_NO_ING_GIVEN
        ERROR_MSG_CMD_LINE_NO_THREE_DIGITS_IMG_NO
        WARNING_MSG_INSUFFICIENT_RESOURCE_CAPACITIES
        EXC_MSG_ERRONEOUS_CONFIG_FILE_CONTENT
        EXC_MSG_CONFIG_FILE_NOT_FOUND
        EXC_MSG_WRONG_NUMBER_ARGUMENTS_CONFIG_FILE
        EXC_MSG_ERROR_WHILE_GETTING_IMAGE_DATA
        EXC_MSG_NOT_ENOUGH_INITIAL_INVENTORY
        EXC_MSG_NOT_ENOUGH_WAREHOUSE_CAPACITY_FOR_INITIALSTOCK
        EXC_MSG_ERROR_WHILE_GENERATING_OPL_OUTPUT
        EXC_MSG_ERROR_WHILE_PREPARING_OPTIMIZATION
        EXC_MSG_ERROR_WHILE_INVOKING_OPTIMIZATION_SERVICE
        EXC_MSG_VOID_SET_OF_RELEVANT_DEMANDS
        EXC_MSG_ONLY_ARTICLES_TO_BUY_IN_DEMAND_SET
        EXC_MSG_SIMULATION_NOT_EXIST
        EXC_MSG_NOT_SINGLE_PLANT
        EXC_MSG_ERROR_WHILE_PERFORMING_DETAILED_ASSIGNMENT
        EXC_MSG_ERROR_WHILE_WRITING_DETAILED_QUANTITIES
        EXC_MSG_ERROR_WHILE_WRITING_EXTENDED_QUANTITIES
        EXC_MSG_ILLEGAL_COMMAND_LINE_PARAMETER
        EXC_MSG_ERROR_WHILE_TRIGGERING_QVS_TASK
        EXC_MSG_UNKNOWN_ERROR_DURING_OPTIMIZATION
        EXC_MSG_INFEASIBILITY
        EXC_MSG_GENERAL_UNKNOWN_ERROR
        SVR_MSG_SVR_SIDE_OPT_LAUNCHED
        SVR_MSG_OPTIMIZATON_OPT_LAUNCHED
        SVR_MSG_OPTIMIZATON_OPT_COMPLETED
        SVR_MSG_OPTIMIZATON_EARLY_LAUNCHED
        SVR_MSG_OPTIMIZATON_EARLY_COMPLETED
        SVR_MSG_OPTIMIZATON_LATE_LAUNCHED
        SVR_MSG_OPTIMIZATON_LATE_COMPLETED
        SVR_MSG_SVR_SIDE_OPT_COMPLETED
        SVR_EXC_OPL_EXECUTION_FAILED
        SVR_EXC_OPL_LOG_WRITING_FAILED
        HST_MSG_EXE_RUNNING
        HST_MSG_SV_RUNNING
        HST_MSG_SV_STOPPED
        HST_EXC_COULD_NOT_START
    End Enum

    Public Interface UserMessageGenerator
        Function getMessage(ByVal msgId As MessageId, ByVal ParamArray parameter As String()) As String
    End Interface


    Public Class UserMessageGeneratorEnglish
        Implements UserMessageGenerator

        Private messages As String() = {
                                        "Optimizer started and running on database {0}.",
                                        "Preparing and checking demand data...",
                                        "Clearing output tables...",
                                        "Getting optimization data...",
                                        "Running remote optimization...",
                                        "Writing results to database...",
                                        "Generating detailed output data ...",
                                        "Writing detailed output data to database...",
                                        "Writing paper demands...",
                                        "Reloading Qliview document...",
                                        "Done.",
                                        "Optimization session completed.",
                                        "{0} Articles with lacking information found.",
                                        "Warehouse {0} with lacking capcity found for {1} optimization",
                                        "Task execution failed.",
                                        "Task execution aborted.",
                                        "Task execution not successful for unknown reason.",
                                        "Other error. Maybe a wrong task password.",
                                        "Task {0} not found.",
                                        "Wrong number of parameters.",
                                        "No number as image number given in command line parameter.",
                                        "Command line parameter cannot contain image number with 3 digits.",
                                        "Solution found but insufficient resource capacities.",
                                        "Erroneous content in CPMS config file.",
                                        "Could not open CPMS config file.",
                                        "Incomplete specification of configuration in CPMS config file.",
                                        "No image found with given number.",
                                        "Not enough initial inventory.",
                                        "Not enough warehouse capacities for initial inventory.",
                                        "Error while writing OPL output: {0}",
                                        "Error while preparing optimization: {0}",
                                        "Error while invoking optimization service: {0}",
                                        "Void set of relevant demands. No optimization possible.",
                                        "Only articles to buy in demand set. No optimization possible.",
                                        "Simulation does not exist.",
                                        "Not a single plant problem.",
                                        "Error while performing detailed assignment of optimized quantities to used demand quantitites: {0}",
                                        "Error while writing detailed optimized quantities: {0}",
                                        "Error while writing extended quantities: {0}",
                                        "Invalid command line parameter: {0}",
                                        "Error while triggering qlikview server task: {0}",
                                        "Unknown error during optimization ocurred: {0}",
                                        "No solution found due to infeasibility.",
                                        "Unknown error: {0}",
                                         "Server side optimization session with Id {0} launched.",
                                        "Optimization for optimal lotsizing launched.",
                                        "Optimization for optimal lotsizing completed.",
                                        "Optimization for early lotsizing launched.",
                                        "Optimization for early lotsizing completed.",
                                        "Optimization for late lotsizing launched.",
                                        "Optimization for late lotsizing completed.",
                                        "Server side optimization session with Id {0} completed.",
                                        "Could not execute {0} due to the following reason: {1}",
                                        "Could not write OPL outputs to log file due to the follwing reason: {0}",
                                        "Host process for Optimizer-Service successfully started and now running under address {0} and {1}. Please press any key for terminating the host process.",
                                        "Host service for Optimizer-Service successfully started and now running under address {0} and {1}.",
                                        "Host service for Optimizer-Service stopped.",
                                        "{0} could not be started: {1}"
                                        }

        Public Sub New()

        End Sub

        Public Function getMessage(ByVal msgId As MessageId, ByVal ParamArray parameter As String()) As String Implements UserMessageGenerator.getMessage

            getMessage = String.Format(messages(Convert.ToInt32(msgId)), parameter)

        End Function
    End Class


End Namespace
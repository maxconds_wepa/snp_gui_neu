﻿
Imports System.Diagnostics
Imports OptiFunctions.Data
Imports OptiFunctions.UserMessages
Imports System.Threading
Namespace Logging

    Public Enum LoggerMsgType
        INFO_MSG
        WARNING_MSG
        ERROR_MSG
    End Enum

    Public Interface Logger

        'Sub openSesssion(ByRef simId As String)
        Sub outputMessage(ByVal type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer)
        ' Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String())
    End Interface

    Public Class LoggerCollection
        Implements Logger
        'Public Const INFO_MSG As Integer = 1
        'Public Const WARNING_MSG As Integer = 2
        'Public Const ERROR_MSG As Integer = 3

        Protected numberOfLoggers As Integer

        Protected lC As Logger()

        Public Sub addLogger(ByRef l As Logger)

            ReDim Preserve lC(numberOfLoggers)
            lC(numberOfLoggers) = l

            numberOfLoggers = numberOfLoggers + 1
        End Sub

        Public Sub outputMessage(ByVal type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer) Implements Logger.outputMessage
            Dim i As Integer

            For i = 0 To numberOfLoggers - 1

                SyncLock (lC(i))
                    lC(i).outputMessage(type, msg, msgId)
                End SyncLock

            Next

        End Sub

        'Public Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String()) Implements Logger.outputUserMessage
        '    Dim i As Integer

        '    For i = 0 To numberOfLoggers - 1

        '        SyncLock (lC(i))
        '            lC(i).outputUserMessage(type, userMsgId, msgParams)
        '        End SyncLock

        '    Next

        'End Sub


    End Class

    Public Class CPMSWitronLogger
        Implements Logger

        Protected Const ISO_8601_FORMAT_STRING As String = "yyyy-MM-ddTHH:mm:ss"

        '  Protected spId As String

        'Constants for logging methods
        Protected Const LOG_TABLE_NAME As String = "OR_IMAGERUN_LOG"
        Protected Const MAX_MSG_STRING_LENGTH As Integer = 255
        Protected Const LOG_TYPE_STRING_INFO As String = "'Info'"
        Protected Const LOG_TYPE_STRING_WARNING As String = "'Warning'"
        Protected Const LOG_TYPE_STRING_ERROR As String = "'ERROR'"

        Protected logContent As DatTable
        Protected runId As String
        Protected usrId As String
        Protected usedLogDb As DataBase

        Public Sub New(ByRef logDb As DataBase, ByRef rId As String, ByRef uId As String)
            ' spId = sId
            runId = rId
            usrId = uId
            usedLogDb = logDb
            logContent = New DatTable
            logContent.numberOfRows = 1
            logContent.numberOfColumns = 6

            ReDim logContent.cells(1, logContent.numberOfColumns - 1)
        End Sub

        Public Sub outputMessage(type As LoggerMsgType, ByRef msg As String, msgId As Integer) Implements Logger.outputMessage
            Dim currentTime As DateTime
            Dim lTab As SQLTable

            currentTime = Now
            Dim strNowSQL As String = "'" & currentTime.ToString(ISO_8601_FORMAT_STRING) & "'" ' ISO 860-Format (unabhängig von Ländereinstellung akzeptiert!)

            logContent.cells(0, 0) = runId
            logContent.cells(0, 1) = "@@spid" 'spId 'simId

            Select Case type
                Case OptiFunctions.Logging.LoggerMsgType.INFO_MSG
                    logContent.cells(0, 2) = LOG_TYPE_STRING_INFO
                Case OptiFunctions.Logging.LoggerMsgType.WARNING_MSG
                    logContent.cells(0, 2) = LOG_TYPE_STRING_WARNING
                Case OptiFunctions.Logging.LoggerMsgType.ERROR_MSG
                    logContent.cells(0, 2) = LOG_TYPE_STRING_ERROR
            End Select

            If msg.Length < MAX_MSG_STRING_LENGTH Then
                logContent.cells(0, 3) = "'" & msg & "'"
            Else
                logContent.cells(0, 3) = "'" & msg.Substring(0, MAX_MSG_STRING_LENGTH - 1).Replace("'", "") & "'" ' Truncate string to 255 chars and avoid ' in strings to be written to SQL-Database
            End If

            logContent.cells(0, 4) = strNowSQL
            logContent.cells(0, 5) = usrId

            lTab = New SQLTable(usedLogDb)
            lTab.setTablenameForWriting(LOG_TABLE_NAME)
            lTab.ArrayToTable(logContent)
        End Sub
    End Class



    Public Class LoggerCollectionBPOptimizer
        Inherits LoggerCollection
        'Public Const INFO_MSG As Integer = 1
        'Public Const WARNING_MSG As Integer = 2
        'Public Const ERROR_MSG As Integer = 3
        Private msgGen As UserMessageGenerator
        ' Private numberOfLoggers As Integer

        'Private lC As Logger()

        Public Sub New(ByRef mG As UserMessageGenerator)
            msgGen = mG
        End Sub
        'Public Sub addLogger(ByRef l As Logger)

        '    ReDim Preserve lC(numberOfLoggers)
        '    lC(numberOfLoggers) = l

        '    numberOfLoggers = numberOfLoggers + 1

        'End Sub

        'Public Sub outputMessage(ByVal type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer) Implements Logger.outputMessage
        '    Dim i As Integer

        '    For i = 0 To numberOfLoggers - 1

        '        SyncLock (lC(i))
        '            lC(i).outputMessage(type, msg, msgId)
        '        End SyncLock

        '    Next

        'End Sub

        Public Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String())
            Dim i As Integer

            For i = 0 To numberOfLoggers - 1

                SyncLock (lC(i))
                    'lC(i).outputUserMessage(type, userMsgId, msgParams)
                    lC(i).outputMessage(type, msgGen.getMessage(userMsgId, msgParams), Convert.ToInt32(userMsgId))
                End SyncLock

            Next

        End Sub


    End Class


    Public Class ScreenLogger
        Implements Logger


        Protected mut As Mutex
        'Private msgGen As UserMessageGenerator
        'Public Sub addExceptionMsg(ByRef msg As String) Implements Logger.addExceptionMsg
        '    Console.WriteLine("Exception occurred:" & msg)
        'End Sub

        'Public Sub addStatusMsg(ByRef msg As String) Implements Logger.addStatusMsg
        '    Console.WriteLine(msg)
        'End Sub

        Public Sub New()
            '   msgGen = mG
            mut = New Mutex()

        End Sub

        Public Sub outputMessage(type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer) Implements Logger.outputMessage

            mut.WaitOne()

            Select Case type
                Case LoggerMsgType.INFO_MSG
                    Console.WriteLine(msg)
                Case LoggerMsgType.WARNING_MSG
                    Console.WriteLine("Warning: " & msg)
                Case LoggerMsgType.ERROR_MSG
                    Console.WriteLine("Error:" & msg)
            End Select

            mut.ReleaseMutex()

        End Sub

        'Public Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String()) Implements Logger.outputUserMessage
        '    outputMessage(type, msgGen.getMessage(userMsgId, msgParams), Convert.ToInt32(userMsgId))
        'End Sub
    End Class

    Public Class SystemEventLogger
        Implements Logger

        Private msgGen As UserMessageGenerator

        Dim eLog As EventLog

        Dim sName As String
        Dim lName As String

        Public Sub New(ByRef sourceName As String, ByRef logName As String, ByRef mG As UserMessageGenerator)
            'Try
            sName = sourceName
            lName = logName

            eLog = New EventLog

            eLog.Source = sourceName '"Sourcetest" 'optiServiceName
            eLog.Log = logName
            msgGen = mG
            'Catch ex As Exception
            'EventLog1.WriteEntry("OnStart-Error:" & ex.Message)
            'End Try
        End Sub

        Public Sub createSource()
            If Not System.Diagnostics.EventLog.SourceExists(sName) Then
                System.Diagnostics.EventLog.CreateEventSource(sName, lName)
            End If

        End Sub

        Public Sub deleteSource()
            If System.Diagnostics.EventLog.SourceExists(sName) Then
                System.Diagnostics.EventLog.DeleteEventSource(sName)
            End If
        End Sub

        Public Sub outputMessage(type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer) Implements Logger.outputMessage
            Select Case type
                Case LoggerMsgType.INFO_MSG
                    eLog.WriteEntry(msg)
                Case LoggerMsgType.WARNING_MSG
                    eLog.WriteEntry("Warning: " & msg)
                Case LoggerMsgType.ERROR_MSG
                    eLog.WriteEntry("Error:" & msg)
            End Select
        End Sub

        'Public Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String()) Implements Logger.outputUserMessage
        '    outputMessage(type, msgGen.getMessage(userMsgId, msgParams), Convert.ToInt32(userMsgId))
        'End Sub
    End Class


    Public Class FileLogger
        Implements Logger


        Private msgGen As UserMessageGenerator
        Private logFile As OptiFunctions.Data.CSV_File
        Private msgTab As DatTable

        Public Sub New(ByRef fileName As String, ByRef mG As UserMessageGenerator)
            'Try
            logFile = New CSV_File(fileName)
            logFile.setIsHeadLine(False)
            logFile.OpenForWriting(True)
            msgTab = New DatTable
            msgTab.numberOfRows = 1
            msgTab.numberOfColumns = 3

            ReDim msgTab.cells(msgTab.numberOfRows - 1, msgTab.numberOfColumns - 1)
            msgGen = mG
        End Sub

       
        Public Sub outputMessage(type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer) Implements Logger.outputMessage

            Dim currentTime As String

            currentTime = Now.ToString("yyyy-MM-ddTHH:mm:ss")
            msgTab.cells(0, 0) = currentTime
            Select Case type
                Case LoggerMsgType.INFO_MSG
                    msgTab.cells(0, 1) = msg
                Case LoggerMsgType.WARNING_MSG
                    msgTab.cells(0, 1) = "Warning: " & msg
                Case LoggerMsgType.ERROR_MSG
                    msgTab.cells(0, 1) = "Error:" & msg
            End Select
            msgTab.cells(0, 2) = Convert.ToString(msgId)
            logFile.ArrayToTable(msgTab)
        End Sub

        'Public Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String()) Implements Logger.outputUserMessage
        '    outputMessage(type, msgGen.getMessage(userMsgId, msgParams), Convert.ToInt32(userMsgId))
        'End Sub

        Public Sub close()
            If logFile IsNot Nothing Then
                logFile.CloseDoc()
            End If
        End Sub
    End Class

    Public Class TableLogger
        Implements Logger

        Private msgGen As UserMessageGenerator

        Public Structure LogEntry
            Dim timeStamp As Date
            Dim msgType As LoggerMsgType
            Dim msg As String
            Dim msgId As Integer
        End Structure

        Public Structure CurrentLog
            Dim log As LogEntry()
            Dim numberOfLogEntries As Integer
        End Structure

        Private numberLogEntries As Integer
        Private logTab As LogEntry()



        Public Sub New(ByRef fileName As String, ByRef mG As UserMessageGenerator)
            'Try
            numberLogEntries = 0
            msgGen = mG
        End Sub


        Public Sub outputMessage(type As LoggerMsgType, ByRef msg As String, ByVal msgId As Integer) Implements Logger.outputMessage

            'Dim currentTime As String

            'currentTime = Now.ToString("yyyy-MM-ddTHH:mm:ss")


            ReDim Preserve logTab(numberLogEntries)

            logTab(numberLogEntries).msgType = type
            logTab(numberLogEntries).timeStamp = Now

            Select Case type
                Case LoggerMsgType.INFO_MSG
                    logTab(numberLogEntries).msg = msg
                Case LoggerMsgType.WARNING_MSG
                    logTab(numberLogEntries).msg = "Warning: " & msg
                Case LoggerMsgType.ERROR_MSG
                    logTab(numberLogEntries).msg = "Error:" & msg
            End Select
            logTab(numberLogEntries).msgId = msgId
            numberLogEntries = numberLogEntries + 1
        End Sub

        'Public Sub outputUserMessage(ByVal type As LoggerMsgType, ByVal userMsgId As MessageId, ParamArray msgParams As String()) Implements Logger.outputUserMessage
        '    outputMessage(type, msgGen.getMessage(userMsgId, msgParams), Convert.ToInt32(userMsgId))
        'End Sub

        Public Function clearLog() As CurrentLog

            Dim cLog As CurrentLog

            cLog = New CurrentLog
            cLog.numberOfLogEntries = numberLogEntries
            cLog.log = logTab
            numberLogEntries = 0

            clearLog = cLog
        End Function

        Public Function peekLog() As CurrentLog

            Dim cLog As CurrentLog

            cLog = New CurrentLog
            cLog.numberOfLogEntries = numberLogEntries
            cLog.log = logTab

            peekLog = cLog
        End Function

    End Class

End Namespace



Public Class Class1

End Class

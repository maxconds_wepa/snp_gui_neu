﻿
Imports System.Globalization

Namespace StringFunctions

    Public Structure StringParam
        Dim placeHolderNumber As Integer
        Dim paramValue As String
    End Structure

    Public Structure StringParamCollection

        Dim numberOfParams As Integer
        Dim sqlParams() As StringParam

    End Structure

    Public Class ParamStringBuilder

        Private usC As CultureInfo

        Public Sub New()
            usC = CultureInfo.CreateSpecificCulture("en-US")
        End Sub
        Public Function finishTemplateStr(ByRef templateStr As String, ByRef params As StringParamCollection) As String
            Dim tmpLine As String

            tmpLine = templateStr
            For i = params.numberOfParams - 1 To 0 Step -1
                If Not IsNothing(params.sqlParams(i).paramValue) Then
                    tmpLine = tmpLine.Replace("#" + Convert.ToString(params.sqlParams(i).placeHolderNumber, usC), params.sqlParams(i).paramValue)
                End If

            Next

            finishTemplateStr = tmpLine
        End Function
    End Class

End Namespace

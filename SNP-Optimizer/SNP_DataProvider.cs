﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimizer_Data;
using System.IO;
using System.Globalization;
using System.Collections;
using System.Threading;
using System.Data;
using System.Diagnostics;
using SNP_WEPA;
using Logging;
using Optimizer_Exceptions;

namespace SNP_Optimizer
{
    public class ProcessFilterEntry
    {
        public enum TypeOfProcess
        {
            Production,
            Transport,
            PrioritizedProduction
        }

        public TypeOfProcess ProcessType;
        public string CustomerName;
        public string CountryName;
        public string ProductName;
        public string ProductionCountryName;
        public string PlantLocationName;
        public string MachineName;
        public bool IsDate;
        public DateTime ValidUntilDate;
    }
    public class ProductionProcessEntry
    {
        public bool IsProcessedEntry;
        public bool IsPrioritizedProcess;
        public ProductionProcess Process;
    }

    public class TransportationProcessEntry
    {
        public bool IsProcessedEntry;
        public TransportationProcess Process;
    }

    public class SingleDestLocationConstrainedProductionProcessEntry
    {
        public bool IsProcessedByGrouping;
        public bool IsProductionProcessedByPrioritizations;
        public bool IsProductionProcessedByFixations;
        public bool IsProductionProcessedByExclusions;
        public bool IsTransportationProcessedByFixations;
        public bool IsTransportationProcessedByExclusions;
        public bool IsProduction;
        public bool IsPrioritizedProduction;
        public bool IsTransport;
        public bool IsSpecialTransport;
        public string ProductName;
        public WEPA_Location CustomerLocation;
        public string CustLocId;
       
        public Dictionary<String, ProductionProcessEntry> AllowedProcesses;
       
        public Dictionary<String, TransportationProcessEntry> AllowedTransportationProcesses;
        public string CustomerChainName;

        public string Key 
        {
            get { return ProductName + CustLocId; }
        }

        public SingleDestLocationConstrainedProductionProcessEntry()
        {
            AllowedProcesses = new Dictionary<String, ProductionProcessEntry>();//List<ProductionProcessEntry>();
            AllowedTransportationProcesses = new Dictionary<String, TransportationProcessEntry>();//List<TransportationProcessEntry>();
            IsProcessedByGrouping=IsProductionProcessedByExclusions = IsProductionProcessedByFixations = IsTransportationProcessedByExclusions = IsTransportationProcessedByFixations=false;
            IsProduction = IsTransport = false;
        }

        public void addProductionProcess(ProductionProcess p)
        {
            if (!AllowedProcesses.ContainsKey(p.Key))
            {
                ProductionProcessEntry currPE;

                currPE = new ProductionProcessEntry();
                currPE.IsProcessedEntry = false;
                currPE.IsPrioritizedProcess = false;
                currPE.Process = p;

                AllowedProcesses.Add(p.Key, currPE);//.Add(currPE);
            }
        }

        public void addTransportationProcess(TransportationProcess tP)
        {
            if (!AllowedTransportationProcesses.ContainsKey(tP.Key))
            {
                TransportationProcessEntry currPE;

                currPE = new TransportationProcessEntry();
                currPE.IsProcessedEntry = false;
                currPE.Process = tP;

                AllowedTransportationProcesses.Add(tP.Key,currPE);
            }
        }

        public bool isEqualProcesses (SingleDestLocationConstrainedProductionProcessEntry cSDLCPPE)
        {
            bool found;
            if (this.ProductName != cSDLCPPE.ProductName || cSDLCPPE.AllowedProcesses.Count != AllowedProcesses.Count)
                return false;

            foreach (KeyValuePair<String,ProductionProcessEntry> currPEKVP in AllowedProcesses)
            {
                found = false;

                if (cSDLCPPE.AllowedProcesses.ContainsKey(currPEKVP.Key))
                {
                    if (cSDLCPPE.AllowedProcesses[currPEKVP.Key].IsPrioritizedProcess == currPEKVP.Value.IsPrioritizedProcess) //Catches the fact that for
                                                                                                                       //prioritized processes IsProcessedEntry=true indicates a prioritized process 
                                                                                                                       //while IsProcessEntry=false indicates non-prioritized process
                        found = true;
                    else
                        return false;
                }
                else
                    return false;
            }
            
            return true;
        }
    }

    public class MultiDestLocationsConstrainedProductionProcessEntry
    {
        public bool IsPrioritizedProduction;
        public string ProductName;
        public Int32 ConstrEntryNum;
        public string CustomerChainName;
        public List<String> CustomerLocIds;
        public List<ProductionProcessEntry> AllowedProcesses;//List<ProductionProcess> AllowedProcesses;

        public MultiDestLocationsConstrainedProductionProcessEntry()
        {
            AllowedProcesses = new List<ProductionProcessEntry>();//new List<ProductionProcess>();
          
            CustomerLocIds = new List<String>();
        }
    }

    public class EmptyTaskProcEnvironment : SubTask.ISubtaskProcEnvironment
    {
        public delegate void Proc(EmptyTaskProcEnvironment sTE);
        protected Mutex mut;
        public Proc callProc;

        protected List<IDisposable> usedResources;

        SubTask parent;

        public SNP_WEPA_Instance usedPI;
        public bool stopSignal;
        public EmptyTaskProcEnvironment()
        {
            usedResources = new List<IDisposable>();

            callProc = null;
            mut = new Mutex();
            stopSignal = false;
        }

        public bool StopSignal
        {
            get
            {
                bool stopS;
                mut.WaitOne();
                stopS = stopSignal;
                mut.ReleaseMutex();

                return stopS;
            }
            set
            {
                mut.WaitOne();
                stopSignal = value;
                mut.ReleaseMutex();
            }
        }

        public void addUsedResource(IDisposable r)
        {
            mut.WaitOne();
            usedResources.Add(r);
            mut.ReleaseMutex();
        }

        public void removeUsedResource(IDisposable r)
        {
            mut.WaitOne();
            usedResources.Remove(r);
            mut.ReleaseMutex();
        }

        public void clearAllUsedResources()
        {
            mut.WaitOne();
            foreach (IDisposable r in usedResources)
            {
                r.Dispose();
            }

            usedResources.Clear();
            mut.ReleaseMutex();
        }

        public void setSubTask(SubTask sT)
        {
            parent = sT;
        }
        protected bool isWithExceptionHandling = true;
        public void start()
        {
            if (callProc != null)
            {
                if (isWithExceptionHandling)
                {
                    try
                    {
                        callProc(this);
                    }
                    catch (Exception e)
                    {
                        clearAllUsedResources();
                        throw e;
                    }
                }
                else
                {
                    callProc(this);
                }
            }

        }

        public SubTask getSubTask(SubTaskScheduler stSched, params SubTask[] precList)
        {
            int i;
            SubTask newSubTask;

            newSubTask = new SubTask(stSched);
            newSubTask.Procedure = this;

            for (i = 0; i < precList.Length; i++)
                newSubTask.addPredecessor(precList[i]);

            return newSubTask;
        }
        protected const int POLLING_INTERVAL = 1;
        public void stop()
        {
            StopSignal = true;

            parent.waitForCompletion();

            clearAllUsedResources();
        }
    }

    public class ProductInformationState
    {
        public string ProductName;
        public string CustomerName;
        public string CountryName;
        public bool HasNoTranportationProcess;
        public bool HasNoProductionProcess;
    }

    public class ProcessInformationState
    {
        public string ProductName;
        public string ResourceName;
        public bool HasNoResourceWithCapacity;
        public bool IsHistoricalProductionNeeded;
        public bool IsNotExistHistoricalProduction;
    }

    public class ResourceInformationState
    {
        public string MachineName;
        public bool HasNoLocation;
        public bool HasNoCapacity;
    }

    public class RawTransportProcess
    {
        public string SourceLocId;
        public string DestLocId;
        public int NumberOfHistoricalTrucks;
        public double AvgCostPerTruck;
        public HashSet<string> ProductsTransported;
        public bool isArtificial = false;
        
        public RawTransportProcess()
        {
            ProductsTransported = new HashSet<string>();
        }
    }

    public class WHSESWEuropeLocation
    {
        public string LocationName;
        public string LocationCountry;
        public string LocationPLZ;
    }


    public class ProductLackingInformationEntry
    {
        public Product product;
        public bool IsLackingTonPerPallet = false;
        public bool IsLackingTransport = false;
        public bool IsLackingProductionProcess = false;
    }

    public class ProductionProcessLackingInformationEntry
    {
        public Product Product;
        public bool IsLackingMachineWithCapacity = false;
    }

    public class WEPA_Location
    {
        public string LocationCountry;
        public string LocationPLZ;
    }

    public class WEPA_PlantInfo
    {
        public int WarehouseCapacity;
        public string LocationName;
    }

    public class WEPA_Resource
    {
        public string LocationName;
        public double CostPerShift;

        public bool IsDetailedCostPerHour;
        public double VariableCostPerHour;
        public double FixedCostPerHour;
        public double CapitalCostPerHour;
        public double TotalCostPerHour;
        public double TotalCostPerShift;
    }

    public class RawDemand
    {
        public string ProductName;
        public string PeriodName;
        public string CustomerName;
        public string CountryName;
        public double Val;
    };

    public class TourData
    {
        public string TourId;
        public double TourNumberPallets;
        public double TourPrice;
        public string SourceLocId;
        public string DestLocId;
        public string Customer;

        public HashSet<string> ProductsOnTour;

        public TourData()
        {
            ProductsOnTour = new HashSet<string>();
        }
    }

    public interface DistanceCostProvider
    {
        double getCostPerUnit(double distance);
    }

    public class LinearDistanceCostProvider:DistanceCostProvider 
    {
        public double FixedPart;
        public double VarPart;

        public LinearDistanceCostProvider(double fP, double vP)
        {
            FixedPart = fP;
            VarPart = vP;
        }
        public double getCostPerUnit(double distance)
        {
            return FixedPart + distance * VarPart;
        }
    }

    public class CustomerProductTour //To determine feasible combinations on a truck to a customer
    {
        public string Product;
        public HashSet<string> TourIds;
        public bool IsSingleProductTour;
        public CustomerProductTour()
        {
            TourIds = new HashSet<string>();
            IsSingleProductTour = false;
        }
    }

    public class ArticleLocationDistribution //To determine distribution of customer product demands to customer locations
    {
        public Dictionary<string, double> NumberPalletsTransportedPerLocation;

        public ArticleLocationDistribution()
        {
            NumberPalletsTransportedPerLocation = new Dictionary<string, double>();
        }
    }

    public class TransportCustomerEntry
    {
        public string FullName;
        public string CompactName;
        public HashSet<string> NameParts;

        public TransportCustomerEntry()
        {
            NameParts = new HashSet<string>();
        }
    }

    public class TransportCustomerCustomerChainMatching
    {
        public string TransportCustomer;
        public string CustomerChain;
    }
    
    public class XLSTable
    {
        public DataTable RawTable;
        public int[] ColNums;
        public int[] RowNums;
        public int FirstRow;

        public enum MatchType
        {
            EXACT,
            SUBSTRING
        }
    }
    public class SNP_DataProvider : SubTaskScheduler.ISubTaskSchedulerEnvironment
    {
        SNP_WEPA_Instance pI;
        DataLoader usedDL;
        
        public const string KEY_COMPONENT_SEPARATOR="@";
        public bool IsPrioritizedEntriesEliminated = false;
        protected string ERROR_INFO_TRUE = "Ja";
        protected string ERROR_INFO_FALSE = "Nein";
        protected string[] productNotConsideredColumnNames = { "Produkt", "Kunde", "Land", "Kein Transportprozess?", "Kein Produktionsprozess?" };
        protected string[] processesNotConsideredColumnNames = { "Produkt", "Maschine", "Keine Maschine mit Schichten?", "Historische Produktion benötigt?", "Kein historische Produktion?" };
        protected string[] resourceNotConsideredColumnNames = { "Maschine", "Kein Standort?", "Keine Schichten?" };
        protected Dictionary<string, WHSESWEuropeLocation> warehouseLocsSWEurope;
       

        public interface IProgressLogger
        {
            void progressCallBack(int progressPercentage);
        }

        IProgressLogger usedProgressLogger;

        public IProgressLogger ProgressLogger
        {
            get { return usedProgressLogger; }
            set { usedProgressLogger = value; }
        }

        public const int NUMBER_PALLETS_PER_TRUCK = 34;
        public const double DEFAULT_KG_PER_PALLET = 10;
        public const int HOURS_PER_SHIFT = 8;
        public const string DUMMY_LOCATION_NAME = "DUMMY_LOC";
        public const double THRESHOLD_ACCEPTANCE_AVG_TRANSPORT_COST = 50;

        protected bool isWarrantExclusionCompatibilityWithSalesPlansFixations=true;
        protected bool isWarrantFeasiblityWithoutInterplantTransports = false;
        protected bool isUseHistoricalProcessIfNoTonnages = true;
        protected bool isUseTotalCostPerShift = false;
        protected bool isUseGivenCustomerSalesToCustomerTransportRelations = true;
        protected bool isConsiderCountryInfoOfDemands = true;
        protected bool isConsiderTourChainAssignments = true;
        protected bool isAddSymmetricICTransportsIfNotExisting = false;
        protected bool isAddCustomerProductRelatedTransportsForDummyCustomers = true;

        protected const double MAX_DBL = Double.MaxValue;

        protected CultureInfo currC;

        protected LoggerCollection loggers;

        protected int numberOfParallelThreads = 1;

        protected Dictionary<string, string> transportStartSubstitions;
        
        protected Dictionary<string, ProductionProcess> preliminaryProductionProcesses;
        protected Dictionary<string, ProductionProcess> tonnageProductionProcesses;
        protected Dictionary<string, Resource> preliminaryResources;
        protected Dictionary<string, Product> preliminaryProducts;
        protected Dictionary<String, HashSet<String>> preliminaryHistoricalProductionProcessesCustomerCountryProduct;

        protected Dictionary<string, ProductLackingInformationEntry> productsWithLackingInformation;
        protected Dictionary<string, ProductionProcessLackingInformationEntry> productionProcessesWithLackingInformation;
        protected Dictionary<string, HistProductionEntry> preliminaryHistoricalProductions;
        protected Dictionary<string, HashSet<String>> customerProductRelatedHistoricalProductionProcesses;
        
        protected HashSet<string> usedPreliminaryResources;
        protected Dictionary<string, RawDemand> rawDemands;
        protected HashSet<string> customersWithDemands;
        protected Dictionary<string, WEPA_Location> wPlantLocations;

        protected Dictionary<string, WEPA_Resource> wResources;

        protected HashSet<string> historicalProductionProcesses;
        protected Dictionary<string, WEPA_PlantInfo> wPlantInfos;
        protected HashSet<string> productsWithProcesses;
        protected Dictionary<string, HashSet<string>> transportCustomerCustomerChainMatchings;
        protected Dictionary<string, TourData> tours; //ok: Für Aufnahme der Gesamtmenge an Touren
        protected Dictionary<string, HashSet<string>> toursForCustomer; //ok: für Warenkörbe gebraucht
        protected Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomerProduct; //ok: für Verteilung der Nachfrage auf Kundenstandorte gebraucht
        protected Dictionary<string, ArticleLocationDistribution> locationDistributionPerCustomer; //ok: für Verteilung der Nachfrage auf Kundenstandorte gebraucht (wenn keine produktspezifischen Transporte vorhanden sind)
        protected XLSTable histTranspTabNE, histTranspTabSW, whseSWEuropeTab;
        protected Dictionary<string, WEPA_Location> wCustomerLocations;
        protected Dictionary<string, List<string>> transportProcessesToLocation; //ok: für Transprte zu Kundenstandort
        protected List<string> currTransportProcessesToDestLocation; //ok: Für aktuelle Transporte zu einem Kundenstandort
        protected Dictionary<string, RawTransportProcess> transportProcesses; //ok: für Transporte
        protected Dictionary<string, HashSet<string>> productsForCustomerLocation; //ok: Für Prüfung bei kundenstandortbezogenen Warenkörben, ob Produkt an den Standort geliefert wird.

        protected HashSet<string> productLocation; //ok: Für Prüfung, ob Fabrikstandort für Produktion überhaupt benötigt wird und damit ein Transport angelegt werden soll.

        protected Dictionary<string, ProductInformationState> productsNotConsidered;
        protected Dictionary<string, ResourceInformationState> resourcesNotConsidered;
        protected Dictionary<string, ProcessInformationState> processesNotConsidered;
        protected Dictionary<string, HashSet<string>> relevantProductsForCustomer;

        protected Dictionary<string, InitialStockValue> preliminaryInitialStocks;
        protected Dictionary<string, TourData> icTours;

        protected List<ProcessFilterEntry> specialFixations;
        protected List<ProcessFilterEntry> specialExclusions;

        protected int firstMonth;
        protected int lastMonth;

        protected bool isIgnoreTrucksPerTour = true;

        protected int totalTasksCompleted;
        protected int totalNumberOfTasks = 16;

        protected Mutex stopSignalMut;
        protected bool isStop;

        protected Mutex schedMut;
        protected SubTaskScheduler usedSTSched;
        Dictionary<String, HashSet<String>> processesAddedCustProdLoc;
        Dictionary<String, HashSet<String>> defaultCustomerWarehousesForCustomerCountryProduct;
        public void completionCallBack(SubTask sender)
        {
            totalTasksCompleted++;

            if (usedProgressLogger != null)
                usedProgressLogger.progressCallBack((int)((double)totalTasksCompleted / ((double)totalNumberOfTasks) * 100));
        }

        public void stopExecution()
        {
            stopSignalMut.WaitOne();
            isStop = true;
            stopSignalMut.ReleaseMutex();
            schedMut.WaitOne();
            if (usedSTSched != null)
                usedSTSched.stopExecution();
            schedMut.ReleaseMutex();
        }

        public bool StopSignal
        {
            get
            {
                bool stopS;

                stopSignalMut.WaitOne();
                stopS = isStop;
                stopSignalMut.ReleaseMutex();

                return stopS;
            }
        }

        public void exceptionCallBack(SubTask sender, Exception e)
        {
        }

        public void checkStopSignal()
        {
            if (StopSignal)
                throw new Optimizer_Exceptions.OptimizerException(OptimizerException.OptimizerExceptionType.ExecutionStoppedOnUserRequest, "");
        }

        protected string buildLocationIdentifier(string country, string postalCode)
        {
            return country + KEY_COMPONENT_SEPARATOR + postalCode; //country + postalCode;//country + KEY_COMPONENT_SEPARATOR + postalCode;
        }

        protected string getLocationIdentifierFromDemandLocationIdentifier (string demLocIdentifier)
        {
            string[] components;

            components = demLocIdentifier.Split(KEY_COMPONENT_SEPARATOR[0]);
            return components[1] + components[2];
        }

        protected string getCustomerChainFromDemandLocationIdentifier(string demLocIdentifier)
        {
            string[] components;

            components = demLocIdentifier.Split(KEY_COMPONENT_SEPARATOR[0]);
            return components[0];
        }

        protected string buildDemandLocIdentifier(string customerChainName, string locationIdentifier)
        {
            return customerChainName + KEY_COMPONENT_SEPARATOR + locationIdentifier;//customerChainName + locationIdentifier; // customerChainName + KEY_COMPONENT_SEPARATOR + locationIdentifier;//KEY_COMPONENT_SEPARATOR +
        }

        protected class TranspConnection
        {
            public string demLocName;
            public WEPA_Location sourceLoc; //string sourceLoc;
            public WEPA_Location destLoc;//string destLoc;
        }

        protected const double transpCostBase=10;
        protected const double varTranspCost=10;
        protected DistanceCostProvider usedDistanceCostProvider;
        protected bool isCheckCountryMissingCustomerTransportConnections = true;

        //double getTransportCostPerUnit(double distance)
        //{
        //    return transpCostBase + distance * varTranspCost;
        //}

        protected bool isSpecialTransportationFixationEntry (ProcessFilterEntry currPFE)
        {
            if (
                currPFE.ProcessType==ProcessFilterEntry.TypeOfProcess.Transport &&
                currPFE.PlantLocationName == ""
                && currPFE.ProductionCountryName == ""
                )
                return true;

            return false;
        }

        protected bool isSpecialProductionFixationEntry(ProcessFilterEntry currPFE)
        {
            if (
                currPFE.ProcessType==ProcessFilterEntry.TypeOfProcess.Production &&
                currPFE.CountryName == "" && 
                currPFE.CustomerName == "" && 
                currPFE.ProductName == ""
                )
                return true;
            return false;
        }

        protected bool isRelevantConstrEntry(
                                             ProcessFilterEntry currPFE, 
                                             String customerChainName, 
                                             String countryName, 
                                             String productName
                                            )
        {
            bool isMatchCustomer, isMatchCountry, isMatchArticle;

            isMatchCustomer = isMatchCountry = isMatchArticle = true;

            if (currPFE.IsDate && currPFE.ValidUntilDate.Month < firstMonth+1 )
                return false;

            if (currPFE.CustomerName != "" && currPFE.CustomerName != customerChainName)
                isMatchCustomer = false;

            if (currPFE.CountryName != "" && currPFE.CountryName != countryName)
                isMatchCountry = false;

            if (currPFE.ProductName != "" && currPFE.ProductName != productName)
                isMatchArticle = false;

            return isMatchArticle && isMatchCountry && isMatchCustomer;
        }

        

        protected bool isRelevantTransportationProcess(ProcessFilterEntry currPFE, TransportationProcessEntry currTPE)
        {
            bool isMatchPlant, isMatchPlantCountry, isMatchResource, isMatchProduct;
            WEPA_Location currPlantLocation;
            
            WEPA_PlantInfo currPlantInfo;

            if (currPFE.IsDate && currPFE.ValidUntilDate.Month < firstMonth && currPFE.ProcessType != ProcessFilterEntry.TypeOfProcess.Transport)
                return false;

            isMatchProduct = isMatchResource = isMatchPlant = isMatchPlantCountry = true;
            
            currPlantInfo = wPlantInfos[currTPE.Process.StartLocation];//[currPlantLocation.LocationCountry + currPlantLocation.LocationPLZ];//[currResource.LocationName];
            currPlantLocation = wPlantLocations[currPlantInfo.LocationName]; //[currPlantInfo.LocationName];
            
            if (currPFE.ProductionCountryName != "" && currPlantLocation.LocationCountry != currPFE.ProductionCountryName)
                isMatchPlantCountry = false;

            if (currPFE.PlantLocationName != "" && currPlantInfo.LocationName != currPFE.PlantLocationName)
                isMatchPlant = false;

            //if (currPFE.ProductName != "" && currPPE.Process.ProductName != currPFE.ProductName)
              //  isMatchProduct = false;

            return isMatchPlant && isMatchPlantCountry;// && isMatchProduct;
        }
       
        protected bool isRelevantProcess(ProcessFilterEntry currPFE, ProductionProcess currPP)
        {
            bool isMatchPlant, isMatchPlantCountry, isMatchResource, isMatchProduct;
            WEPA_Location currPlantLocation;
            WEPA_Resource currResource;
            WEPA_PlantInfo currPlantInfo;

            if (currPFE.IsDate && currPFE.ValidUntilDate.Month < firstMonth && currPFE.ProcessType != ProcessFilterEntry.TypeOfProcess.Production)
                return false;

            isMatchProduct = isMatchResource = isMatchPlant = isMatchPlantCountry = true;
            currResource = wResources[currPP.ResourceName];

            currPlantLocation = wPlantLocations[currResource.LocationName];
            currPlantInfo = wPlantInfos[currPlantLocation.LocationCountry + currPlantLocation.LocationPLZ];

            if (currPFE.ProductionCountryName != "" && currPlantLocation.LocationCountry != currPFE.ProductionCountryName)
                isMatchPlantCountry = false;

            if (currPFE.MachineName != "" && currPFE.MachineName != currPP.ResourceName)
                isMatchResource = false;

            if (currPFE.PlantLocationName != "" && currPlantInfo.LocationName != currPFE.PlantLocationName)
                isMatchPlant = false;

            if (currPFE.ProductName != "" && currPP.ProductName != currPFE.ProductName)
                isMatchProduct = false;

            return isMatchPlant && isMatchResource && isMatchPlantCountry && isMatchProduct;
        }
        protected bool isCustomerProcessConnectionInSalesPlan(String customerChainName,
                                                               String productName,
                                                                WEPA_Location custLocation,
                                                               ProductionProcess currPP)
        {
            String usedDemandEntryKey;
            HashSet<String> currSalesPlanEntries;
            HashSet<String> histProdProcesses;

            //Retrieve for each customer production entry from the fixations / exclusions matrix the corresponding sales plan entry
            usedDemandEntryKey = productName + buildDemandLocIdentifier(customerChainName, buildLocationIdentifier(custLocation.LocationCountry, custLocation.LocationPLZ));

            currSalesPlanEntries = usedSalesplanEntriesForDemand[usedDemandEntryKey];
            
            foreach (String sKey in currSalesPlanEntries)
            {
                if (preliminaryHistoricalProductionProcessesCustomerCountryProduct.ContainsKey(sKey))//if there is a valid historical production process from sales plan
                {
                    histProdProcesses = preliminaryHistoricalProductionProcessesCustomerCountryProduct[sKey];
                    if (histProdProcesses.Contains(currPP.Key))
                        return true;
                }
            }

            return false;
        }

        protected void addProductionProcessPriorities()
        {
            bool isPrioritizedProcessesFound;
            int i = 0;
            foreach (ProcessFilterEntry currPFE in specialFixations)
            {
               
                if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.PrioritizedProduction)
                {
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
                    {
                        if (
                            currCPPEKVP.Value.IsProduction || currCPPEKVP.Value.IsPrioritizedProduction
                        )
                        {
                            if (isRelevantConstrEntry(
                                                         currPFE,
                                                         currCPPEKVP.Value.CustomerChainName,
                                                         currCPPEKVP.Value.CustomerLocation.LocationCountry,
                                                         currCPPEKVP.Value.ProductName
                                                         )
                               )
                            {
                                isPrioritizedProcessesFound = false;
                                foreach (KeyValuePair<String, ProductionProcessEntry> currPPEKVP in currCPPEKVP.Value.AllowedProcesses)
                                {
                                    if (isRelevantProcess(currPFE, currPPEKVP.Value.Process))
                                    {
                                        currPPEKVP.Value.IsPrioritizedProcess = true;//IsProcessedEntry = true;    //Mark process as fixed
                                        isPrioritizedProcessesFound = true;
                                        //currCPPEKVP.Value.IsProductionProcessedByFixations = true;  //Mark fixation entry as processed by the fixations procedure
                                    }
                                }

                                if (isPrioritizedProcessesFound)
                                {
                                    currCPPEKVP.Value.IsProductionProcessedByPrioritizations = true;
                                }

                            }
                        }
                    }
                }
            }
        }
        
        protected void determineProductionProcessConstraintsFixations ()
        {
            Int32 constrEntryNum;
            Dictionary<String, ProductionProcessEntry> fixedEntries;
            Dictionary<String,TransportationProcessEntry> fixedTEntries;
            bool isSpecialFixationEntry;

            constrEntryNum = 0;
            foreach (ProcessFilterEntry currPFE in specialFixations)
            {
                if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Production)
                {
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
                    {
                        if (
                            currCPPEKVP.Value .IsProduction && 
                            !currCPPEKVP.Value.IsProductionProcessedByExclusions //Not already excluded by the exclusions matrix
                            )
                        {   
                            if (isRelevantConstrEntry(
                                                          currPFE, 
                                                          currCPPEKVP.Value.CustomerChainName, 
                                                          currCPPEKVP.Value.CustomerLocation.LocationCountry, 
                                                          currCPPEKVP.Value.ProductName
                                                          )
                                )
                            {   
                                isSpecialFixationEntry=isSpecialProductionFixationEntry(currPFE);
                                if (
                                        !isSpecialFixationEntry //Ignore fixation entries with special meaning
                                    )
                                {
                                    foreach (KeyValuePair<String,ProductionProcessEntry> currPPEKVP in currCPPEKVP.Value.AllowedProcesses)
                                    {
                                        if (isRelevantProcess(currPFE, currPPEKVP.Value.Process))//uncommment if prioritized processes are always kept in the set of feasible processes || currPPEKVP.Value.IsPrioritizedProcess)
                                        {
                                            currPPEKVP.Value.IsProcessedEntry = true;    //Mark process as fixed
                                            currCPPEKVP.Value.IsProductionProcessedByFixations = true;  //Mark fixation entry as processed by the fixations procedure
                                        }
                                    }
                                   
                                }
                                else if (isSpecialFixationEntry)
                                {
                                   
                                    foreach (KeyValuePair<String, ProductionProcessEntry> currPPEKVP in currCPPEKVP.Value.AllowedProcesses)
                                    {
                                        if (
                                            isRelevantProcess(currPFE, currPPEKVP.Value.Process) &&//uncommment if prioritized processes are always kept in the set of feasible processes || currPPEKVP.Value.IsPrioritizedProcess)
                                            isCustomerProcessConnectionInSalesPlan(
                                                                                    currCPPEKVP.Value.CustomerChainName, 
                                                                                    currCPPEKVP.Value.ProductName, 
                                                                                    currCPPEKVP.Value.CustomerLocation, 
                                                                                    currPPEKVP.Value.Process
                                                                                   )
                                            )
                                        {
                                            currPPEKVP.Value.IsProcessedEntry = true;
                                            currCPPEKVP.Value.IsProductionProcessedByFixations = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Transport)
                {
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
                    {
                        if (
                            currCPPEKVP.Value.IsTransport &&
                            !currCPPEKVP.Value.IsTransportationProcessedByExclusions //Not already excluded by the exclusions matrix
                            )
                        {
                            if (
                                isRelevantConstrEntry(
                                                      currPFE,
                                                      currCPPEKVP.Value.CustomerChainName,
                                                      currCPPEKVP.Value.CustomerLocation.LocationCountry,
                                                      currCPPEKVP.Value.ProductName
                                                      )
                                )
                            {
                                if (!isSpecialTransportationFixationEntry(currPFE))
                                {
                                    foreach (KeyValuePair<String, TransportationProcessEntry> currTPEKVP in currCPPEKVP.Value.AllowedTransportationProcesses)
                                    {
                                        if (isRelevantTransportationProcess(currPFE,
                                                                            currTPEKVP.Value
                                                                            )
                                            )
                                        {
                                            currTPEKVP.Value.IsProcessedEntry = true;//Mark process as fixed
                                            currCPPEKVP.Value.IsTransportationProcessedByFixations = true;  //Mark fixation entry as processed by the fixations procedure
                                        }
                                    }
                                   
                                }
                                else
                                {

                                }
                            }
                        }
                    }
                }
                constrEntryNum++;
            }
           
            foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
            {
                if (currCPPEKVP.Value.IsProductionProcessedByFixations)
                {
                    fixedEntries = new Dictionary<String,ProductionProcessEntry>();//List<ProductionProcessEntry>();
                    foreach (KeyValuePair<String,ProductionProcessEntry> currPPEKVP in currCPPEKVP.Value.AllowedProcesses)
                    {
                        if (currPPEKVP.Value.IsProcessedEntry)       //add only processes marked as fixed
                        {
                            fixedEntries.Add(currPPEKVP.Key, currPPEKVP.Value);
                        }
                        else
                        {
                            if (currPPEKVP.Value.IsPrioritizedProcess)
                            {
                                IsPrioritizedEntriesEliminated = true;
                            }
                        }
                    }

                    currCPPEKVP.Value.AllowedProcesses = fixedEntries;
                }
            }
           
            foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
            {
                if (currCPPEKVP.Value.IsTransportationProcessedByFixations) //Consider only fixation entries processed by this fixation procedure
                {
                    fixedTEntries =new Dictionary<String, TransportationProcessEntry>();//new List<TransportationProcessEntry>();

                    foreach (KeyValuePair<String,TransportationProcessEntry> currTPE in currCPPEKVP.Value.AllowedTransportationProcesses)
                    {
                        if (currTPE.Value.IsProcessedEntry)
                        {
                            fixedTEntries.Add(currTPE.Key,currTPE.Value);     //add only processes marked as fixed
                        }
                    }
                    
                    currCPPEKVP.Value.AllowedTransportationProcesses = fixedTEntries;
                }
            }
        }

        protected void determineProductionProcessConstraintsExclusions()
        {
            Int32 constrEntryNum;
            Dictionary<String,ProductionProcessEntry> fixedEntries;
            Dictionary<String,TransportationProcessEntry> fixedTEntries;

            constrEntryNum = 0;
            foreach (ProcessFilterEntry currPFE in specialExclusions)
            {
                if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Production)
                {
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
                    {  
                        if (currCPPEKVP.Value.IsProduction && 
                            !currCPPEKVP.Value.IsProductionProcessedByFixations //Not already fixed by the fixations matrix
                            )
                        {
                            if (isRelevantConstrEntry(currPFE, currCPPEKVP.Value.CustomerChainName, currCPPEKVP.Value.CustomerLocation.LocationCountry, currCPPEKVP.Value.ProductName)
                                )
                            {
                                foreach (KeyValuePair<String,ProductionProcessEntry> currPPEKVP in currCPPEKVP.Value.AllowedProcesses)
                                {
                                    if (isRelevantProcess(currPFE, currPPEKVP.Value.Process)) //uncommment if prioritized processes are always kept in the set of feasible processes && !currPPEKVP.Value.IsPrioritizedProcess)
                                    {
                                        currPPEKVP.Value.IsProcessedEntry = true;    //Mark process as excluded
                                        currCPPEKVP.Value.IsProductionProcessedByExclusions = true; //Mark fixation entry as processed by the exclusions procedure
                                    }
                                }
                               
                            }
                        }
                    }
                }
                else if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Transport)
                {
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
                    {
                        if (currCPPEKVP.Value.IsTransport &&
                            !currCPPEKVP.Value.IsTransportationProcessedByFixations //Not already fixed by the fixations matrix
                            )
                        {
                            if (isRelevantConstrEntry(currPFE, currCPPEKVP.Value.CustomerChainName, currCPPEKVP.Value.CustomerLocation.LocationCountry, currCPPEKVP.Value.ProductName)
                                )
                            {
                                foreach (KeyValuePair<String,TransportationProcessEntry> currTPEKVP in currCPPEKVP.Value.AllowedTransportationProcesses)
                                {
                                    if (isRelevantTransportationProcess(currPFE, currTPEKVP.Value))
                                    {
                                        currTPEKVP.Value.IsProcessedEntry = true;    //Mark process as excluded
                                        currCPPEKVP.Value.IsTransportationProcessedByExclusions = true; //Mark fixation entry as processed by the exclusions procedure
                                    }
                                }
                                
                            }
                        }
                    }
                }
                constrEntryNum++;
            }

            foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
            {
                if (currCPPEKVP.Value.IsProductionProcessedByExclusions)
                {
                    fixedEntries = new Dictionary<String,ProductionProcessEntry>();
                    foreach (KeyValuePair<String,ProductionProcessEntry> currPPEKVP in currCPPEKVP.Value.AllowedProcesses)
                    {
                        if (!currPPEKVP.Value.IsProcessedEntry)      //Add only processes not marked as excluded
                        {
                            fixedEntries.Add(currPPEKVP.Key, currPPEKVP.Value);
                        }
                        else
                        {
                            if (currPPEKVP.Value.IsPrioritizedProcess)
                            {
                                IsPrioritizedEntriesEliminated = true;
                            }
                        }
                    }
                  
                    currCPPEKVP.Value.AllowedProcesses = fixedEntries;
                }
            }

            foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currCPPEKVP in constrSDLProdProcesses)
            {
                if (currCPPEKVP.Value.IsTransportationProcessedByExclusions)    //Consider only fixation entries processed by this exclusion procedure
                {
                    fixedTEntries = new Dictionary<String,TransportationProcessEntry>();
                    foreach (KeyValuePair<String,TransportationProcessEntry> currTPEKVP in currCPPEKVP.Value.AllowedTransportationProcesses)
                    {
                        if (!currTPEKVP.Value.IsProcessedEntry)//Add only processes not marked as excluded
                        {
                            fixedTEntries.Add(currTPEKVP.Key,currTPEKVP.Value);
                        }
                    }
                   
                    currCPPEKVP.Value.AllowedTransportationProcesses = fixedTEntries;
                }
            }
        }
        
        protected List<MultiDestLocationsConstrainedProductionProcessEntry> constrMDLProdProcesses;
        protected Dictionary<String, SingleDestLocationConstrainedProductionProcessEntry> constrSDLProdProcesses;
        protected void groupSingleDestLocationProductionProcesses()
        {
            int i, j, numberOfProcesses;
            MultiDestLocationsConstrainedProductionProcessEntry currMDLEntry;
            SingleDestLocationConstrainedProductionProcessEntry[] sDLCPPEs;
            constrMDLProdProcesses = new List<MultiDestLocationsConstrainedProductionProcessEntry>();

            sDLCPPEs = new SingleDestLocationConstrainedProductionProcessEntry[constrSDLProdProcesses.Count];
            i = 0;
            foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currSDLCPPEKV in constrSDLProdProcesses)
            {
                if (currSDLCPPEKV.Value.IsProductionProcessedByFixations || currSDLCPPEKV.Value.IsProductionProcessedByExclusions || currSDLCPPEKV.Value.IsProductionProcessedByPrioritizations)
                {
                    sDLCPPEs[i] = currSDLCPPEKV.Value;
                    i++;
                }
                else
                {
                    i += 0;
                }

            }
            numberOfProcesses = i;
            for (i = 0; i < numberOfProcesses; i++)
            {
                if (!sDLCPPEs[i].IsProcessedByGrouping &&
                    (sDLCPPEs[i].IsProduction ||
                        sDLCPPEs[i].IsProductionProcessedByPrioritizations
                     )
                    )
                {
                    currMDLEntry = new MultiDestLocationsConstrainedProductionProcessEntry();

                    currMDLEntry.IsPrioritizedProduction = sDLCPPEs[i].IsPrioritizedProduction;
                    currMDLEntry.CustomerLocIds.Add(sDLCPPEs[i].CustLocId);

                    foreach (KeyValuePair<String, ProductionProcessEntry> currPEKVP in sDLCPPEs[i].AllowedProcesses)
                    {
                        currMDLEntry.AllowedProcesses.Add(currPEKVP.Value);//.Process);
                    }

                    sDLCPPEs[i].IsProcessedByGrouping = true;

                    for (j = i + 1; j < numberOfProcesses; j++)
                    {
                        if ((sDLCPPEs[j].IsProduction ||
                             sDLCPPEs[i].IsProductionProcessedByPrioritizations
                             )
                             && sDLCPPEs[i].isEqualProcesses(sDLCPPEs[j])
                            )
                        {
                            sDLCPPEs[j].IsProcessedByGrouping = true;
                            currMDLEntry.CustomerLocIds.Add(sDLCPPEs[j].CustLocId);
                        }
                    }

                    constrMDLProdProcesses.Add(currMDLEntry);
                }
            }
        }

        protected class CustomerProductEntry {
            public String CustomerChain;
            public String CustomerLocationId;
            public String ProductName;
        };

        protected Dictionary<String, CustomerProductEntry> customerProductsLeadingToInfeasibleBaskets;

        protected void checkTransportCompatibilityWithBaskets()
        {  
            List<String> constrainedProducts;
            String currCustProdName;
            List<HashSet<String>> currBasketList;
            List<HashSet<String>> newBasketList;
            List<HashSet<String>> basketsToBeAdded;
            List<HashSet<String>> basketsToBeRemoved;
            HashSet <String> newBasket;
            SingleDestLocationConstrainedProductionProcessEntry currEntry;
            HashSet<String> commonPlantLocations, currPlantLocations ;
            HashSet<String> basketBase;
            HashSet<String> constrainedProductsWithoutBasket;
            Int32 currNumberOfConstrainedProducts;
            Int32[] currConstrainedProductIndices;
            String[] constrainedProductNames;
            Int32 i,j;
           
            bool isAllProductsWithoutBasket;

            constrainedProducts = new List<string>();
            commonPlantLocations = new HashSet<string>();
            currPlantLocations = new HashSet<string>();
            basketsToBeAdded = new List<HashSet<string>>();
            basketsToBeRemoved = new List<HashSet<string>>();
            constrainedProductsWithoutBasket = new HashSet<string>();
            basketBase=new HashSet<string> ();
           
            foreach (KeyValuePair<string, BasketSet> currPrelimBaskets in preliminaryBasketSets)
            {
                currBasketList = currPrelimBaskets.Value.getAllBaskets();
               
                basketsToBeAdded.Clear();
                basketsToBeRemoved.Clear();
               
                foreach (HashSet<String> currBasket in currBasketList)
                {
                    constrainedProducts.Clear();
                    basketBase.Clear();

                    foreach (String currProdName in currBasket)
                    {
                        currCustProdName = currProdName + currPrelimBaskets.Key;

                        if (constrSDLProdProcesses.ContainsKey(currCustProdName))
                        {
                            currEntry = constrSDLProdProcesses[currCustProdName];
                            if (currEntry.IsTransport)
                                constrainedProducts.Add(currProdName);
                        }
                    }
                    
                    foreach (String currProdName in currBasket)
                    {
                        if (!constrainedProducts.Contains(currProdName))
                            basketBase.Add(currProdName);
                    }

                    if (constrainedProducts.Count > 1)
                    {
                       
                        basketsToBeRemoved.Add(currBasket);

                       
                        constrainedProductNames = new String[constrainedProducts.Count];

                        i = 0;
                        foreach (String cProd in constrainedProducts)
                        {
                            constrainedProductNames[i] = cProd;
                            constrainedProductsWithoutBasket.Add(cProd);
                            i++;
                        }

                        currConstrainedProductIndices = new Int32[constrainedProducts.Count];

                        for (currNumberOfConstrainedProducts = constrainedProducts.Count; currNumberOfConstrainedProducts > 0; currNumberOfConstrainedProducts--)
                        {
                         
                            for (i = 0; i < currNumberOfConstrainedProducts; i++)
                            {
                                currConstrainedProductIndices[i] = i;
                            }

                            do
                            {
                                isAllProductsWithoutBasket = true;
                                for (i = 0; i < currNumberOfConstrainedProducts; i++)
                                {
                                    if (!constrainedProductsWithoutBasket.Contains(constrainedProductNames[currConstrainedProductIndices[i]]))
                                        isAllProductsWithoutBasket = false;
                                }

                                if (isAllProductsWithoutBasket)
                                {
                                    currCustProdName = constrainedProductNames[currConstrainedProductIndices[0]] + currPrelimBaskets.Key;

                                    currEntry = constrSDLProdProcesses[currCustProdName];
                                    commonPlantLocations.Clear();
                                    foreach (KeyValuePair<String,TransportationProcessEntry> currPEKVP in currEntry.AllowedTransportationProcesses)
                                    {
                                        if (!commonPlantLocations.Contains(currPEKVP.Value.Process.StartLocation))
                                            commonPlantLocations.Add(currPEKVP.Value.Process.StartLocation);
                                    }
                                    
                                    for (i = 1; i < currNumberOfConstrainedProducts; i++)
                                    {
                                        currCustProdName = constrainedProductNames[currConstrainedProductIndices[i]] + currPrelimBaskets.Key;
                                        currEntry = constrSDLProdProcesses[currCustProdName];
                                        currPlantLocations.Clear();
                                        foreach (KeyValuePair<String,TransportationProcessEntry> currPEKVP in currEntry.AllowedTransportationProcesses)
                                        {
                                            if (!currPlantLocations.Contains(currPEKVP.Value.Process.StartLocation))
                                                currPlantLocations.Add(currPEKVP.Value.Process.StartLocation);
                                        }
                                       
                                        commonPlantLocations.IntersectWith(currPlantLocations);
                                        if (commonPlantLocations.Count == 0)
                                        {
                                            if (currNumberOfConstrainedProducts == constrainedProducts.Count)
                                            {
                                                foreach (String currProdName in currBasket)
                                                {
                                                    String currKey;
                                                    currKey = currProdName + currPrelimBaskets.Key;
                                                    if (constrSDLProdProcesses.ContainsKey(currKey))
                                                    {
                                                        currEntry = constrSDLProdProcesses[currKey];
                                                        if (currEntry.IsTransport)
                                                        {
                                                            if (!customerProductsLeadingToInfeasibleBaskets.ContainsKey(currKey))
                                                            {
                                                                CustomerProductEntry currE;

                                                                currE = new CustomerProductEntry();
                                                                currE.ProductName = currProdName;
                                                                currE.CustomerChain = getCustomerChainFromDemandLocationIdentifier(currPrelimBaskets.Key);
                                                                currE.CustomerLocationId = getLocationIdentifierFromDemandLocationIdentifier(currPrelimBaskets.Key);
                                                                customerProductsLeadingToInfeasibleBaskets.Add(currKey, currE);
                                                            }
                                                        }
                                                    }
                                                    
                                                }
                                            }

                                            break;
                                        }
                                    }

                                    if (commonPlantLocations.Count > 0 || currNumberOfConstrainedProducts == 1)
                                    {
                                      
                                        newBasket = new HashSet<string>();
 
                                        foreach (String productName in basketBase)
                                            newBasket.Add(productName);

                                        for (i = 0; i < currNumberOfConstrainedProducts; i++)
                                        {
                                            newBasket.Add(constrainedProductNames[currConstrainedProductIndices[i]]);
                                            constrainedProductsWithoutBasket.Remove(constrainedProductNames[currConstrainedProductIndices[i]]);
                                        }
                                      
                                        basketsToBeAdded.Add(newBasket);
                                    }
                                }
                                i = currNumberOfConstrainedProducts;

                                do
                                {
                                    
                                    i--;
                                    currConstrainedProductIndices[i]++;
                                    
                                        for (j = i+1; j < currNumberOfConstrainedProducts; j++)
                                            currConstrainedProductIndices[j] = currConstrainedProductIndices[i] + (j - i);
                                    
                                }
                                while (
                                        i > 0 && currConstrainedProductIndices[i] == i + 1 + constrainedProducts.Count - currNumberOfConstrainedProducts
                                    );
                               
                            }
                            while (!(i == 0 && currConstrainedProductIndices[0] == 1 + constrainedProducts.Count - currNumberOfConstrainedProducts));

                            if (constrainedProductsWithoutBasket.Count == 0)
                                break;
                            
                        }
                    }
                }

                foreach (HashSet<String> currBasket in basketsToBeRemoved)
                    currBasketList.Remove(currBasket);

                foreach (HashSet<String> currBasket in basketsToBeAdded)
                    currBasketList.Add(currBasket);

                newBasketList = pruneBasketCandidateSet(currBasketList);
                currBasketList.Clear();
                
                currBasketList.AddRange(newBasketList);
                
            }
        }
       

        protected void checkFixationExclusionCompatibilityWithSalesPlansFixations()
        {   
            foreach (KeyValuePair<String,SingleDestLocationConstrainedProductionProcessEntry> currsDCPPEKVP in constrSDLProdProcesses)
            {

                if (currsDCPPEKVP.Value.IsProduction)
                {
                    String currCustProdKey;
                    HashSet<String> histProdProcesses;
                    bool found;
                    String usedDemandEntryKey;
                    HashSet<String> currSalesPlanEntries;

                    //Retrieve for each customer production entry from the fixations / exclusions matrix the corresponding sales plan entry
                    usedDemandEntryKey = currsDCPPEKVP.Value.ProductName + 
                        buildDemandLocIdentifier(currsDCPPEKVP.Value.CustomerChainName, buildLocationIdentifier( currsDCPPEKVP.Value.CustomerLocation.LocationCountry, currsDCPPEKVP.Value.CustomerLocation.LocationPLZ));
                    
                    currSalesPlanEntries = usedSalesplanEntriesForDemand[usedDemandEntryKey];

                    foreach (String sKey in currSalesPlanEntries)
                    {
                        if (preliminaryHistoricalProductionProcessesCustomerCountryProduct.ContainsKey(sKey))//if there is a valid historical production process from sales plan
                        {
                            histProdProcesses = preliminaryHistoricalProductionProcessesCustomerCountryProduct[sKey];
                            
                            foreach (String currProcessName in histProdProcesses)
                            {
                                found = false;
                               //Check for each historical production process whether it is exists in the set of admissible production processes after consideration of the exclusions / fixations
                                //matrix
                                //If it does not exist yet but exists in general for the respective production add it to the set of admissible production processes
                                //and to the set of additional production processes for the respective customer location product (used for output in a separate file for information purposes).
                                foreach (KeyValuePair<String,ProductionProcessEntry> currPPEKVP in currsDCPPEKVP.Value.AllowedProcesses)
                                {
                                    if (currPPEKVP.Value.Process.Key == currProcessName)
                                        found = true;
                                }
                               
                                if (!found)
                                {
                                    IndexedItem<ProductionProcess> currPI;

                                    currPI = pI.getProductionProcess(currProcessName);
                                    if (currPI != null)
                                    {
                                        currsDCPPEKVP.Value.addProductionProcess(pI.getProductionProcess(currProcessName).item);
                                        HashSet<String> processes;
                                        currCustProdKey = currsDCPPEKVP.Value.CustomerChainName + currsDCPPEKVP.Value.ProductName + currsDCPPEKVP.Value.CustomerLocation.LocationCountry;
                                        if (processesAddedCustProdLoc.ContainsKey(currCustProdKey))
                                            processes = processesAddedCustProdLoc[currCustProdKey];
                                        else
                                        {
                                            processes = new HashSet<string>();
                                            processesAddedCustProdLoc.Add(currCustProdKey, processes);
                                        }

                                        if (!processes.Contains(currProcessName))
                                            processes.Add(currProcessName);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void determineProductionAndTransportProcessConstraints()
        {
            WEPA_Location currCustLocation;
            HashSet<string> currProductDemandLocations;
            List<IndexedItem<Product>> allProducts;
            String currCustChain;
            String currCustProdIdentifier;
            SingleDestLocationConstrainedProductionProcessEntry currDLCPPE;
            Int32 constrEntryNum;
            string currLOcId;
            HashSet<String> histProdProcesses;
            IndexedItem<ProductionProcess> currProdProcess;
            IndexedItem<Resource> currResource;
            HashSet<String> defaultCustomerWarehouseLocs;
            List<IndexedItem<ProductionProcess>> relevantProcesses;
            String sKey;
            bool isSpecialTransportsOK;
            IndexedItem<PlantLocation> currPlantLoc;
            bool isSpecialProdEntry;
            constrSDLProdProcesses = new Dictionary<String, SingleDestLocationConstrainedProductionProcessEntry>();

            allProducts = pI.getAllProducts();
           // i = 0;
            foreach (IndexedItem<Product> currP in allProducts)
            {   
                currProductDemandLocations = productDemandLocations[currP.item.ProductName];
                
                foreach (string currDemLoc in currProductDemandLocations)
                {   
                    if (pI.getCustomerLocation(currDemLoc) != null)
                    {   
                        currLOcId=getLocationIdentifierFromDemandLocationIdentifier(currDemLoc);
                        currCustChain=getCustomerChainFromDemandLocationIdentifier (currDemLoc );

                        if (wCustomerLocations.ContainsKey(currLOcId))//Catch dummy locations
                        {
                            currCustLocation = wCustomerLocations[currLOcId];
                            constrEntryNum = 0;
                            foreach (ProcessFilterEntry currPFE in specialFixations)
                            {   
                                isSpecialProdEntry = isSpecialProductionFixationEntry(currPFE);
                                if (currPFE.ProcessType != ProcessFilterEntry.TypeOfProcess.PrioritizedProduction &&
                                    isRelevantConstrEntry(currPFE, 
                                                          currCustChain, 
                                                          currCustLocation.LocationCountry, 
                                                          currP.item.ProductName
                                                          )
                                    )
                                {
                                    currCustProdIdentifier = currP.item.ProductName + currDemLoc;
                                    //For each customer product where a special fixation entry exists we add an fixation entry which accepts the contrained production processes
                                    if (!constrSDLProdProcesses.ContainsKey (currCustProdIdentifier))
                                    {
                                        currDLCPPE = new SingleDestLocationConstrainedProductionProcessEntry();
                                        currDLCPPE.ProductName = currP.item.ProductName;
                                        currDLCPPE.CustomerChainName = currCustChain;
                                        currDLCPPE.CustomerLocation = currCustLocation;
                                        currDLCPPE.CustLocId = currDemLoc;

                                        if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Production)
                                            currDLCPPE.IsProduction = true;
                                       
                                        else
                                            currDLCPPE.IsTransport = true;
                                        
                                        constrSDLProdProcesses.Add(currCustProdIdentifier, currDLCPPE);
                                    }
                                    else
                                        currDLCPPE = constrSDLProdProcesses[currCustProdIdentifier];

                                    if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Production)
                                        currDLCPPE.IsProduction = true;
                                    
                                    else
                                    {
                                        currDLCPPE.IsTransport = true;
                                        if (isSpecialTransportationFixationEntry(currPFE))
                                        {
                                            currDLCPPE.IsSpecialTransport = true;
                                        }
                                        else
                                            currDLCPPE.IsSpecialTransport = false;
                                    }
                                }

                                constrEntryNum++;
                            }

                            constrEntryNum = 0;
                            foreach (ProcessFilterEntry currPFE in specialExclusions)
                            {  
                                if (isRelevantConstrEntry(currPFE, currCustChain, currCustLocation.LocationCountry, currP.item.ProductName))
                                {
                                    currCustProdIdentifier = currP.item.ProductName + currDemLoc;

                                    //For each customer product where a special exclusion entry exists we add an fixation entry which accepts the contrained production processes
                                    if (!constrSDLProdProcesses.ContainsKey(currCustProdIdentifier))
                                    {
                                        currDLCPPE = new SingleDestLocationConstrainedProductionProcessEntry();
                                        currDLCPPE.ProductName = currP.item.ProductName;
                                        currDLCPPE.CustomerChainName = currCustChain;
                                        currDLCPPE.CustomerLocation = currCustLocation;
                                        currDLCPPE.CustLocId = currDemLoc;

                                        constrSDLProdProcesses.Add(currCustProdIdentifier, currDLCPPE);
                                    }
                                    else
                                        currDLCPPE = constrSDLProdProcesses[currCustProdIdentifier];

                                    if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.Production)
                                        currDLCPPE.IsProduction = true;
                                    else
                                    {
                                        currDLCPPE.IsSpecialTransport = false;
                                        currDLCPPE.IsTransport = true;
                                    }
                                }

                                constrEntryNum++;
                            }
                        }
                    }
                }
            }
            relevantProcesses = pI.getAllProductionProcesses();
           
            //Add to each fixation entry the set of all relevant production and transportation processes that are production processes to obtain the product or transportation
            //ending at the respective customer location
            foreach (KeyValuePair<String, SingleDestLocationConstrainedProductionProcessEntry> currSDLCPPEKVP in constrSDLProdProcesses)
            {
                if (currSDLCPPEKVP.Value.IsProduction)
                {
                    foreach (IndexedItem<ProductionProcess> currProcessII in relevantProcesses)
                    {
                        if (currProcessII.item.ProductName == 
                            currSDLCPPEKVP.Value.ProductName
                            )
                        {
                            currSDLCPPEKVP.Value.addProductionProcess(currProcessII.item);
                        }
                    }
                }
            
                if (currSDLCPPEKVP.Value.IsTransport)
                {
                    isSpecialTransportsOK = false;

                    if (currSDLCPPEKVP.Value.IsSpecialTransport)
                    {   
                        sKey=currSDLCPPEKVP.Value.CustomerChainName+currSDLCPPEKVP.Value.ProductName;
                        
                        if (isConsiderCountryInfoOfDemands)
                            sKey+=currSDLCPPEKVP.Value.CustomerLocation.LocationCountry;

                        if (defaultCustomerWarehousesForCustomerCountryProduct.ContainsKey(sKey))//if there is a valid historical production process from sales plan
                        {
                            defaultCustomerWarehouseLocs = defaultCustomerWarehousesForCustomerCountryProduct[sKey];
                           
                            foreach (string currWHLC in defaultCustomerWarehouseLocs)
                            {
                                currPlantLoc = pI.getPlantLocation(currWHLC);

                                if (currPlantLoc != null)
                                {
                                    foreach (KeyValuePair<string, TransportationProcess> tProcKV in preliminaryTransportationProcesses)
                                    {
                                        if (tProcKV.Value.DestLocation ==
                                            currSDLCPPEKVP.Value.CustLocId &&
                                            tProcKV.Value.StartLocation == currWHLC
                                            )
                                            isSpecialTransportsOK = true;

                                        currSDLCPPEKVP.Value.addTransportationProcess(tProcKV.Value);
                                    }
                                }
                            }
                        }

                        if (!isSpecialTransportsOK)
                        {
                            if (preliminaryHistoricalProductionProcessesCustomerCountryProduct.ContainsKey(sKey))//if there is a valid historical production process from sales plan
                            {
                                histProdProcesses = preliminaryHistoricalProductionProcessesCustomerCountryProduct[sKey];

                                foreach (string currPP in histProdProcesses)
                                {
                                    currProdProcess = pI.getProductionProcess(currPP);

                                    if (currProdProcess != null)
                                    {
                                        currResource = pI.getResource(currProdProcess.item.ResourceName);

                                        foreach (KeyValuePair<string, TransportationProcess> tProcKV in preliminaryTransportationProcesses)
                                        {
                                            if (tProcKV.Value.DestLocation ==
                                                currSDLCPPEKVP.Value.CustLocId &&
                                                tProcKV.Value.StartLocation == currResource.item.LocationName
                                                )
                                            {
                                                isSpecialTransportsOK = true;
                                                currSDLCPPEKVP.Value.addTransportationProcess(tProcKV.Value);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!currSDLCPPEKVP.Value.IsSpecialTransport ||!isSpecialTransportsOK)
                    {
                        foreach (KeyValuePair<string, TransportationProcess> tProcKV in preliminaryTransportationProcesses)
                        {
                            if (tProcKV.Value.DestLocation ==
                                currSDLCPPEKVP.Value.CustLocId
                                )
                                currSDLCPPEKVP.Value.addTransportationProcess(tProcKV.Value);
                        }
                    }
                }
            }
            //For the case where neither from the exclusions nor the fixations matrix entries are available for entries with prioritized production we add 
            //entries in order to reflect prioritized production

            foreach (IndexedItem<Product> currP in allProducts)
            {
                currProductDemandLocations = productDemandLocations[currP.item.ProductName];
                if (currP.item.ProductName=="039596")
                    constrEntryNum = 0;
                foreach (string currDemLoc in currProductDemandLocations)
                {
                    if (pI.getCustomerLocation(currDemLoc) != null)
                    {
                        currLOcId = getLocationIdentifierFromDemandLocationIdentifier(currDemLoc);
                        currCustChain = getCustomerChainFromDemandLocationIdentifier(currDemLoc);

                        if (wCustomerLocations.ContainsKey(currLOcId))//Catch dummy locations
                        {
                            currCustLocation = wCustomerLocations[currLOcId];
                            constrEntryNum = 0;
                            foreach (ProcessFilterEntry currPFE in specialFixations)
                            {
                                isSpecialProdEntry = isSpecialProductionFixationEntry(currPFE);
                                if (currPFE.ProcessType == ProcessFilterEntry.TypeOfProcess.PrioritizedProduction &&
                                    //!isSpecialProductionFixationEntry(currPFE) && //Ignore entries with special meaning
                                    isRelevantConstrEntry(currPFE,
                                                          currCustChain,
                                                          currCustLocation.LocationCountry,
                                                          currP.item.ProductName
                                                          )
                                    )
                                {
                                    currCustProdIdentifier = currP.item.ProductName + currDemLoc;
                                    //For each customer product where a special fixation entry exists we add an fixation entry which accepts the contrained production processes
                                    if (!constrSDLProdProcesses.ContainsKey(currCustProdIdentifier))
                                    {
                                        currDLCPPE = new SingleDestLocationConstrainedProductionProcessEntry();
                                        currDLCPPE.ProductName = currP.item.ProductName;
                                        currDLCPPE.CustomerChainName = currCustChain;
                                        currDLCPPE.CustomerLocation = currCustLocation;
                                        currDLCPPE.CustLocId = currDemLoc;
                                        
                                        currDLCPPE.IsPrioritizedProduction = true;
                                        
                                        constrSDLProdProcesses.Add(currCustProdIdentifier, currDLCPPE);
                                    }                                    
                                }

                                constrEntryNum++;
                            }
                        }
                    }
                }
            }

            foreach (KeyValuePair<String, SingleDestLocationConstrainedProductionProcessEntry> currSDLCPPEKVP in constrSDLProdProcesses)
            {
                if (currSDLCPPEKVP.Value.IsPrioritizedProduction)
                {
                    foreach (IndexedItem<ProductionProcess> currProcessII in relevantProcesses)
                    {
                        if (currProcessII.item.ProductName ==
                            currSDLCPPEKVP.Value.ProductName
                            )
                        {
                            currSDLCPPEKVP.Value.addProductionProcess(currProcessII.item);
                        }
                    }
                }
            }
            addProductionProcessPriorities();
            determineProductionProcessConstraintsFixations();   //Process fixations matrix
            determineProductionProcessConstraintsExclusions();  //Process exclusions matrix

            if (isWarrantExclusionCompatibilityWithSalesPlansFixations)
                checkFixationExclusionCompatibilityWithSalesPlansFixations();   

            checkTransportCompatibilityWithBaskets();
            groupSingleDestLocationProductionProcesses();
        }

        public SNP_WEPA_Instance getProblemInstance(
            DataLoader dL,
            string productsNotConsideredFN,
            string processesNotConsideredFN,
            string resourcesNotConsideredFN,
            string missingTranspCFN,
            string customerProductsInfeasibleBasketsFN,
            string fixationExclusionProductionProductsFN,
            string fixationExclusionTransportationProductsFN,
            string processesAddedFN,
            int maxNumberOfThreads,
            bool isCheckCMCTC,
            DistanceSource distanceData,
            bool isUseExternalDistanceData,
            DistanceCostProvider usedDCP,
            bool isLoadTotalCostPerShift,
            bool isCheckFixationExclusionCompatibilityWithSalesPlansFixations
            )
        {
            int i;
            string objKey;
            string msg;
            Dictionary<string, TranspConnection> missingTranspConnections;
            WEPA_PlantInfo currPLInfo;
            PlantLocation currPlantLoc;
            EmptyTaskProcEnvironment locsLocsMachinesPE, demandDataPE, shiftDataPE, tonnageDataPE, baseDataAndWorkingPlansPE, tpNEEuropePE, tpSWEuropePE, whseSWEuropePE;//, costPerShiftPE
            SubTask locsLocsMachinesST, demandDataST, shiftDataST, tonnageDataST, baseDataAndWorkingPlansST, tpNEEuropeST, tpSWEuropeST, whseSWEuropeST;// costPerShiftST,
            EmptyTaskProcEnvironment transportDataPE;
            SubTask transportDataST;
            EmptyTaskProcEnvironment productionProcessesPE;
            SubTask productionProcessesST;
            EmptyTaskProcEnvironment transportProcessesPE;
            SubTask transportProcessesST;
            EmptyTaskProcEnvironment customerDemandsPE;
            SubTask customerDemandsST;
            EmptyTaskProcEnvironment basketsPE;
            SubTask basketsST;
            EmptyTaskProcEnvironment interplantTransportsPE;
            SubTask interplantTransportsST;
            SubTaskScheduler.SUBTASK_SCHED_RESULT result;
            ProductionProcess currProcess;
            Resource currResource;
            SubTask initialStockST;
            EmptyTaskProcEnvironment initialStockPE;
            SubTask transportStartSubstitutionsST;
            EmptyTaskProcEnvironment transportStartSubstitutionsPE;
            SubTask specialFixationST;
            EmptyTaskProcEnvironment specialFixationPE;
            SubTask specialExclusionST;
            EmptyTaskProcEnvironment specialExclusionPE;
            List<IndexedItem<PlantLocation>> usedPlantLocations;
            DataTable productsNotConsideredTab, processesNotConsideredTab, resourcesNotConsideredTab;
            CSV_File productsNotConsideredFile, processesNotConsideredFile, resourcesNotConsideredFile, processesAddedFile;
            WEPA_Location currPlantLocation;
            WEPA_Resource currWResource;
            string currResourceLocId;
            string[] pNCRow;
            WEPA_PlantInfo currPlantInfo;
            TranspConnection currTConn;
            string[] mTPRow;
            DataTable missingTransportsTab;
            CSV_File missingTranspCFile;
            string startId, destId;
            double nDistance;
            string currTranspProcName;
            TransportationProcess currTransProcess;

            isUseTotalCostPerShift = isLoadTotalCostPerShift;
            isWarrantExclusionCompatibilityWithSalesPlansFixations = isCheckFixationExclusionCompatibilityWithSalesPlansFixations;
            isCheckCountryMissingCustomerTransportConnections = isCheckCMCTC;
            missingTranspConnections = new Dictionary<string, TranspConnection>();
            customerProductsLeadingToInfeasibleBaskets = new Dictionary<string, CustomerProductEntry>();
            usedDistanceCostProvider = usedDCP;
            numberOfParallelThreads = maxNumberOfThreads;
            totalTasksCompleted = 0;
            IsPrioritizedEntriesEliminated = false;

            processesAddedCustProdLoc = new Dictionary<string, HashSet<string>>();

            if (usedProgressLogger != null)
                usedProgressLogger.progressCallBack((int)((double)totalTasksCompleted / ((double)totalNumberOfTasks) * 100));
            try
            {
                msg = "Initialisierungen...";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

                usedDL = dL;

                usedDL.getFirstLastMonth(ref firstMonth, ref lastMonth, loggers, currC);

                productsNotConsidered = new Dictionary<string, ProductInformationState>();
                processesNotConsidered = new Dictionary<string, ProcessInformationState>();
                resourcesNotConsidered = new Dictionary<string, ResourceInformationState>();
               
                schedMut.WaitOne();
                usedSTSched = new SubTaskScheduler();
                usedSTSched.SchedEnv = this;
                usedSTSched.MaxNumberRunningInParallel = numberOfParallelThreads;
                schedMut.ReleaseMutex();

                checkStopSignal();

                productsWithLackingInformation = new Dictionary<string, ProductLackingInformationEntry>();
                productionProcessesWithLackingInformation = new Dictionary<string, ProductionProcessLackingInformationEntry>();
                pI = new SNP_WEPA_Instance();

                msg = "Fertig.";
                loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

                locsLocsMachinesPE = new EmptyTaskProcEnvironment();
                locsLocsMachinesPE.callProc = getLocationsAndLocsMachines;
                locsLocsMachinesST = locsLocsMachinesPE.getSubTask(usedSTSched);
                usedSTSched.addSubTask(locsLocsMachinesST);

                interplantTransportsPE = new EmptyTaskProcEnvironment();
                interplantTransportsPE.callProc = getInterplantTransports;
                interplantTransportsPE.usedPI = pI;
                interplantTransportsST = interplantTransportsPE.getSubTask(usedSTSched);
                interplantTransportsST.addPredecessor(locsLocsMachinesST);
                usedSTSched.addSubTask(interplantTransportsST);

                transportStartSubstitutionsPE = new EmptyTaskProcEnvironment();
                transportStartSubstitutionsPE.callProc = getTransportStartSubstitutions;
                transportStartSubstitutionsST = transportStartSubstitutionsPE.getSubTask(usedSTSched);
                transportStartSubstitutionsST.addPredecessor(locsLocsMachinesST);
                usedSTSched.addSubTask(transportStartSubstitutionsST);

                demandDataPE = new EmptyTaskProcEnvironment();
                demandDataPE.callProc = getDemandData;
                demandDataPE.usedPI = pI;
                demandDataST = demandDataPE.getSubTask(usedSTSched);
                demandDataST.addPredecessor(locsLocsMachinesST);
                usedSTSched.addSubTask(demandDataST);

                initialStockPE = new EmptyTaskProcEnvironment();
                initialStockPE.callProc = getInitialStocks;
                initialStockST = initialStockPE.getSubTask(usedSTSched);
                initialStockST.addPredecessor(demandDataST);
                usedSTSched.addSubTask(initialStockST);

                shiftDataPE = new EmptyTaskProcEnvironment();
                shiftDataPE.callProc = getShiftData;
                shiftDataPE.usedPI = pI;
                shiftDataST = shiftDataPE.getSubTask(usedSTSched, locsLocsMachinesST);
                shiftDataST.addPredecessor(demandDataST);
                usedSTSched.addSubTask(shiftDataST);

                tonnageDataPE = new EmptyTaskProcEnvironment();
                tonnageDataPE.callProc = getTonnageData;
                tonnageDataPE.usedPI = pI;
                tonnageDataST = tonnageDataPE.getSubTask(usedSTSched, shiftDataST);
                tonnageDataST.addPredecessor(shiftDataST);
                usedSTSched.addSubTask(tonnageDataST);

                baseDataAndWorkingPlansPE = new EmptyTaskProcEnvironment();
                baseDataAndWorkingPlansPE.callProc = getBaseDataAndWorkingPlans;
                baseDataAndWorkingPlansPE.usedPI = pI;
                baseDataAndWorkingPlansST = baseDataAndWorkingPlansPE.getSubTask(usedSTSched, tonnageDataST, demandDataST);
                baseDataAndWorkingPlansST.addPredecessor(tonnageDataST);
                baseDataAndWorkingPlansST.addPredecessor(locsLocsMachinesST);

                usedSTSched.addSubTask(baseDataAndWorkingPlansST);


                tpNEEuropePE = new EmptyTaskProcEnvironment();
                tpNEEuropePE.callProc = readTransportsNE;
                tpNEEuropeST = tpNEEuropePE.getSubTask(usedSTSched);

                tpSWEuropePE = new EmptyTaskProcEnvironment();
                tpSWEuropePE.callProc = readTransportsSW;
                tpSWEuropeST = tpSWEuropePE.getSubTask(usedSTSched);

                whseSWEuropePE = new EmptyTaskProcEnvironment();
                whseSWEuropePE.callProc = readWHSESWEurope;
                whseSWEuropeST = whseSWEuropePE.getSubTask(usedSTSched);

                usedSTSched.addSubTask(tpNEEuropeST);
                usedSTSched.addSubTask(tpSWEuropeST);
                usedSTSched.addSubTask(whseSWEuropeST);

                transportDataPE = new EmptyTaskProcEnvironment();

                transportDataPE.callProc = getTransportData;
                transportDataPE.usedPI = pI;
                transportDataST = transportDataPE.getSubTask(usedSTSched);

                transportDataST.addPredecessor(tpNEEuropeST);
                transportDataST.addPredecessor(tpSWEuropeST);
                transportDataST.addPredecessor(transportStartSubstitutionsST);
                transportDataST.addPredecessor(interplantTransportsST);
                transportDataST.addPredecessor(whseSWEuropeST);
                transportDataST.addPredecessor(demandDataST);
                transportDataST.addPredecessor(baseDataAndWorkingPlansST);

                usedSTSched.addSubTask(transportDataST);

                productionProcessesPE = new EmptyTaskProcEnvironment();
                productionProcessesPE.callProc = prepareProductionProcesses;
                productionProcessesST = productionProcessesPE.getSubTask(usedSTSched);
                productionProcessesST.addPredecessor(transportDataST);
                usedSTSched.addSubTask(productionProcessesST);

                transportProcessesPE = new EmptyTaskProcEnvironment();
                transportProcessesPE.callProc = prepareTransportationProcesses;
                transportProcessesPE.usedPI = pI;
                transportProcessesST = transportProcessesPE.getSubTask(usedSTSched);
                transportProcessesST.addPredecessor(transportDataST);
                usedSTSched.addSubTask(transportProcessesST);

                customerDemandsPE = new EmptyTaskProcEnvironment();
                customerDemandsPE.callProc = prepareCustomerDemands;
                customerDemandsPE.usedPI = pI;
                customerDemandsST = customerDemandsPE.getSubTask(usedSTSched);
                customerDemandsST.addPredecessor(transportProcessesST);
                usedSTSched.addSubTask(customerDemandsST);

                basketsPE = new EmptyTaskProcEnvironment();
                basketsPE.callProc = prepareBaskets;
                basketsPE.usedPI = pI;
                basketsST = basketsPE.getSubTask(usedSTSched);
                basketsST.addPredecessor(customerDemandsST);
                usedSTSched.addSubTask(basketsST);

                specialFixationPE = new EmptyTaskProcEnvironment();
                specialFixationPE.callProc = loadSpecialFixations;
                specialFixationPE.usedPI = pI;
                specialFixationST = specialFixationPE.getSubTask(usedSTSched);
                usedSTSched.addSubTask(specialFixationST);

                specialExclusionPE = new EmptyTaskProcEnvironment();
                specialExclusionPE.callProc = loadSpecialExclusions;
                specialExclusionPE.usedPI = pI;
                specialExclusionST = specialExclusionPE.getSubTask(usedSTSched);
                usedSTSched.addSubTask(specialExclusionST);

                checkStopSignal();
                result = usedSTSched.execSubTaskSet();
                checkStopSignal();

                if (result == SubTaskScheduler.SUBTASK_SCHED_RESULT.FINISHED_OK)
                {
                    i = 0;

                    foreach (KeyValuePair<string, WEPA_Location> wPLKVP in wPlantLocations)
                    {
                        currResourceLocId = wPLKVP.Value.LocationCountry + wPLKVP.Value.LocationPLZ;

                        if (!isWarrantFeasiblityWithoutInterplantTransports && pI.getPlantLocation(currResourceLocId) == null)
                        {
                            currPlantLoc = new PlantLocation();
                            currPlantLoc.LocationName = currResourceLocId;

                            pI.addPlantLocation(currPlantLoc);
                        }
                    }

                    foreach (KeyValuePair<string, ProductionProcess> kP in preliminaryProductionProcesses)
                    {
                        currProcess = kP.Value;
                        currWResource = wResources[currProcess.ResourceName];

                        currPlantLocation = wPlantLocations[currWResource.LocationName];//[currResourceLocName];
                        currResourceLocId = currPlantLocation.LocationCountry + currPlantLocation.LocationPLZ;
                      
                        if (pI.getProduct(currProcess.ProductName) != null && pI.getPlantLocation(currResourceLocId) != null)
                            pI.addProductionProcess(currProcess);
                    }

                    usedPlantLocations = pI.getAllPlantLocations();
                    foreach (IndexedItem<PlantLocation> pL in usedPlantLocations)
                    {
                        currPlantInfo = wPlantInfos[pL.item.LocationName];
                        pL.item.WarehouseCapacity = currPlantInfo.WarehouseCapacity;
                    }

                    foreach (KeyValuePair<string, Resource> kR in preliminaryResources)
                    {
                        currResource = kR.Value;

                        objKey = kR.Key;
                        if (usedPreliminaryResources.Contains(objKey))
                        {
                            pI.addResource(currResource);
                        }
                    }

                    foreach (KeyValuePair<string, HistProductionEntry> cHPET in preliminaryHistoricalProductions)
                    {
                        HistProductionEntry currHPE = cHPET.Value;
                        
                        if (pI.getProductionProcess(currHPE.ProcessName) != null)
                            pI.addHistoricalProduction(currHPE);
                    }

                    foreach (KeyValuePair<string, InitialStockValue> iV in preliminaryInitialStocks)
                    {
                        if (pI.getProduct(iV.Value.ProductName) != null && pI.getPlantLocation(iV.Value.LocationName) != null)
                        {
                            pI.addInitialStock(iV.Value);
                        }
                    }
                    foreach (KeyValuePair<string, BasketSet> bKV in preliminaryBasketSets)
                    {
                        pI.addBasketSet(bKV.Value);
                    }
                    List<IndexedItem<Product>> allProducts = pI.getAllProducts();
                    WEPA_Location currCustLocation;
                    foreach (IndexedItem<Product> currP in allProducts)
                    {
                        HashSet<string> currProductDemandLocations;
                        HashSet<string> currProductProdLocations;

                        //************************************************************************************************************************************************************************************************
                        //***********************************Hier noch einmal prüfen, ob Kundenstandorte in den Schlüssel in productsWithBaskets mit aufgenommen werden sollen********************************************
                        //************************************************************************************************************************************************************************************************

                        //Add connection between plant and customer location to the set of missing transportations to be filled via google maps


                        if (!productsWithBaskets.Contains(currP.item.ProductName)) 
                        {
                            currProductDemandLocations = productDemandLocations[currP.item.ProductName];
                            currProductProdLocations = productProductionLocations[currP.item.ProductName];

                            foreach (string currDemLoc in currProductDemandLocations)
                            {
                                if (pI.getCustomerLocation(currDemLoc) != null)
                                {
                                    foreach (string currProdLoc in currProductProdLocations)
                                    {
                                        currTranspProcName = currProdLoc + currDemLoc;

                                        if (!preliminaryTransportationProcesses.ContainsKey(currTranspProcName) || 
                                                (
                                                preliminaryTransportationProcesses[currTranspProcName].CostPerTruck == -1
                                                )
                                            )
                                        {
                                            currPLInfo = wPlantInfos[currProdLoc];

                                            currPlantLocation = wPlantLocations[currPLInfo.LocationName];
                                            currCustLocation = wCustomerLocations[getLocationIdentifierFromDemandLocationIdentifier(currDemLoc)];

                                            if (
                                                (
                                                    (
                                                        isCheckCountryMissingCustomerTransportConnections && 
                                                        currPlantLocation.LocationCountry == currCustLocation.LocationCountry
                                                     ) 
                                                     ||
                                                    !isCheckCountryMissingCustomerTransportConnections
                                                 )
                                                && !missingTranspConnections.ContainsKey(currTranspProcName)
                                                )
                                            {
                                                currTConn = new TranspConnection();
                                                currTConn.sourceLoc = currPlantLocation;
                                                currTConn.destLoc = currCustLocation;
                                                currTConn.demLocName = currDemLoc;
                                                missingTranspConnections.Add(currTranspProcName, currTConn);
                                                if (isUseExternalDistanceData)
                                                    distanceData.addStartDest(currPlantLocation.LocationCountry, currPlantLocation.LocationPLZ, currCustLocation.LocationCountry, currCustLocation.LocationPLZ);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                   
                    foreach (string aTSPName in artificialTransportProcesses)
                    {
                       
                        TransportationProcess currTP;
                        currTP = preliminaryTransportationProcesses[aTSPName];
                        if (!missingTranspConnections.ContainsKey(aTSPName))
                        {
                            currTConn = new TranspConnection();
                            currPLInfo = wPlantInfos[currTP.StartLocation];//[currTPI.item.StartLocation];

                            currPlantLocation = wPlantLocations[currPLInfo.LocationName];
                            currCustLocation = wCustomerLocations[getLocationIdentifierFromDemandLocationIdentifier(currTP.DestLocation)];//(currTPI.item.DestLocation)];

                            currTConn.sourceLoc = currPlantLocation;//currTPI.item.StartLocation;
                            currTConn.destLoc = currCustLocation;//currTPI.item.DestLocation;
                            currTConn.demLocName = currTP.DestLocation;//currTPI.item.DestLocation;
                            missingTranspConnections.Add(aTSPName, currTConn);
                            if (isUseExternalDistanceData)
                                distanceData.addStartDest(currPlantLocation.LocationCountry, currPlantLocation.LocationPLZ, currCustLocation.LocationCountry, currCustLocation.LocationPLZ);
                        }
                      
                    }
                 
                    productsNotConsideredTab = new DataTable();
                    pNCRow = new string[productNotConsideredColumnNames.Length];
                    for (i = 0; i < productNotConsideredColumnNames.Length; i++)
                        productsNotConsideredTab.Columns.Add(productNotConsideredColumnNames[i]);

                    foreach (KeyValuePair<string, ProductInformationState> pE in productsNotConsidered)
                    {
                        pNCRow[0] = pE.Value.ProductName;
                        pNCRow[1] = pE.Value.CustomerName;
                        pNCRow[2] = pE.Value.CountryName;

                        if (pE.Value.HasNoProductionProcess)
                            pNCRow[3] = ERROR_INFO_TRUE;
                        else
                            pNCRow[3] = ERROR_INFO_FALSE;

                        if (pE.Value.HasNoTranportationProcess)
                            pNCRow[4] = ERROR_INFO_TRUE;
                        else
                            pNCRow[4] = ERROR_INFO_FALSE;

                        productsNotConsideredTab.Rows.Add(pNCRow);
                    }

                    checkStopSignal();

                    productsNotConsideredFile = new CSV_File(productsNotConsideredFN);
                    productsNotConsideredFile.IsHeadLine = true;
                    productsNotConsideredFile.CSVDelimiter = currC.TextInfo.ListSeparator; ;
                    productsNotConsideredFile.openForWriting(false);
                    productsNotConsideredFile.writeData(productsNotConsideredTab);
                    productsNotConsideredFile.closeDoc();

                    processesNotConsideredTab = new DataTable();
                    pNCRow = new string[processesNotConsideredColumnNames.Length];
                    for (i = 0; i < processesNotConsideredColumnNames.Length; i++)
                        processesNotConsideredTab.Columns.Add(processesNotConsideredColumnNames[i]);

                    foreach (KeyValuePair<string, ProcessInformationState> prcE in processesNotConsidered)
                    {
                        pNCRow[0] = prcE.Value.ProductName;
                        pNCRow[1] = prcE.Value.ResourceName;

                        if (prcE.Value.HasNoResourceWithCapacity)
                            pNCRow[2] = ERROR_INFO_TRUE;
                        else
                            pNCRow[2] = ERROR_INFO_FALSE;

                        if (prcE.Value.IsHistoricalProductionNeeded)
                            pNCRow[3] = ERROR_INFO_TRUE;
                        else
                            pNCRow[3] = ERROR_INFO_FALSE;

                        if (prcE.Value.IsNotExistHistoricalProduction)
                            pNCRow[4] = ERROR_INFO_TRUE;
                        else
                            pNCRow[4] = ERROR_INFO_FALSE;

                        processesNotConsideredTab.Rows.Add(pNCRow);
                    }

                    processesNotConsideredFile = new CSV_File(processesNotConsideredFN);
                    processesNotConsideredFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    processesNotConsideredFile.IsHeadLine = true;
                    processesNotConsideredFile.openForWriting(false);
                    processesNotConsideredFile.writeData(processesNotConsideredTab);
                    processesNotConsideredFile.closeDoc();

                    pNCRow = new string[resourceNotConsideredColumnNames.Length];
                    resourcesNotConsideredTab = new DataTable();

                    for (i = 0; i < resourceNotConsideredColumnNames.Length; i++)
                        resourcesNotConsideredTab.Columns.Add(resourceNotConsideredColumnNames[i]);

                    foreach (KeyValuePair<string, ResourceInformationState> rIST in resourcesNotConsidered)
                    {
                        pNCRow[0] = rIST.Key;
                        if (rIST.Value.HasNoLocation)
                            pNCRow[1] = ERROR_INFO_TRUE;
                        else
                            pNCRow[1] = ERROR_INFO_FALSE;


                        if (rIST.Value.HasNoCapacity)
                            pNCRow[2] = ERROR_INFO_TRUE;
                        else
                            pNCRow[2] = ERROR_INFO_FALSE;

                        resourcesNotConsideredTab.Rows.Add(pNCRow);
                    }

                    resourcesNotConsideredFile = new CSV_File(resourcesNotConsideredFN);
                    resourcesNotConsideredFile.IsHeadLine = true;
                    resourcesNotConsideredFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    resourcesNotConsideredFile.openForWriting(false);
                    resourcesNotConsideredFile.writeData(resourcesNotConsideredTab);
                    resourcesNotConsideredFile.closeDoc();

                    mTPRow = new string[2];

                    if (isUseExternalDistanceData)
                        distanceData.fetchDistances();

                    missingTransportsTab = new DataTable();
                    missingTransportsTab.Columns.Add("Plantlocation");
                    missingTransportsTab.Columns.Add("Customerlocation");

                    foreach (KeyValuePair<string, TranspConnection> currTranspC in missingTranspConnections)
                    {
                        startId = currTranspC.Value.sourceLoc.LocationCountry + currTranspC.Value.sourceLoc.LocationPLZ;
                        destId = currTranspC.Value.destLoc.LocationCountry + currTranspC.Value.destLoc.LocationPLZ;

                        if (isUseExternalDistanceData)
                        {
                            nDistance = distanceData.distance(
                                                              currTranspC.Value.sourceLoc.LocationCountry, 
                                                              currTranspC.Value.sourceLoc.LocationPLZ, 
                                                              currTranspC.Value.destLoc.LocationCountry, 
                                                              currTranspC.Value.destLoc.LocationPLZ
                                                              );
                            
                            if (nDistance >= 0)
                            {
                               
                                if (!preliminaryTransportationProcesses.ContainsKey(startId + currTranspC.Value.demLocName))//(cTPI == null)
                                {
                                    currTransProcess = new TransportationProcess();
                                    currTransProcess.StartLocation = startId;
                                    currTransProcess.DestLocation = currTranspC.Value.demLocName;
                               
                                    preliminaryTransportationProcesses.Add(currTransProcess.Key, currTransProcess);
                                }
                                else
                                {
                                    currTransProcess = preliminaryTransportationProcesses[startId + currTranspC.Value.demLocName];//cTPI.item;
                                }

                                currTransProcess.CostPerTruck = usedDistanceCostProvider.getCostPerUnit(nDistance);//getTransportCostPerUnit(nDistance);
                            }
                        }
                        mTPRow[0] = startId;
                        mTPRow[1] = destId;//currTranspC.Value.destLoc;

                        missingTransportsTab.Rows.Add(mTPRow);
                    }

                    missingTranspCFile = new CSV_File(missingTranspCFN);
                    missingTranspCFile.IsHeadLine = true;
                    missingTranspCFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    missingTranspCFile.openForWriting(false);
                    missingTranspCFile.writeData(missingTransportsTab);
                    missingTranspCFile.closeDoc();


                    //  msg = "Gesamte Laufzeit:" + sw.Elapsed;
                    loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

                    determineProductionAndTransportProcessConstraints();

                    CSV_File customerProductsInfeasibleBasketsFile;
                    DataTable cPIFBTab;

                    cPIFBTab = new DataTable();
                    cPIFBTab.Columns.Add("Kundenkette");
                    cPIFBTab.Columns.Add("Standort");
                    cPIFBTab.Columns.Add("Produkt");
                    mTPRow = new string[3];
                    foreach (KeyValuePair<string, CustomerProductEntry> currCPEKV in customerProductsLeadingToInfeasibleBaskets)
                    {
                        mTPRow[0] = currCPEKV.Value.CustomerChain;
                        mTPRow[1] = currCPEKV.Value.CustomerLocationId;
                        mTPRow[2] = currCPEKV.Value.ProductName;
                        cPIFBTab.Rows.Add(mTPRow);
                    }

                    customerProductsInfeasibleBasketsFile = new CSV_File(customerProductsInfeasibleBasketsFN);
                    customerProductsInfeasibleBasketsFile.IsHeadLine = true;
                    customerProductsInfeasibleBasketsFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    customerProductsInfeasibleBasketsFile.openForWriting(false);
                    customerProductsInfeasibleBasketsFile.writeData(cPIFBTab);
                    customerProductsInfeasibleBasketsFile.closeDoc();

                    CSV_File fixationExclusionProductionProductsFile;
                    DataTable fixationExclusionProductionProductsTab;
                    //mTPRow = new string[3];
                    fixationExclusionProductionProductsTab = new DataTable();
                    fixationExclusionProductionProductsTab.Columns.Add("Produkt");
                    fixationExclusionProductionProductsTab.Columns.Add("Kundenkette");
                    fixationExclusionProductionProductsTab.Columns.Add("Standort");
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currKVP in constrSDLProdProcesses)
                    {
                        if (currKVP.Value.IsProduction)
                        {
                            mTPRow[0] = currKVP.Value.ProductName;
                            mTPRow[1] = currKVP.Value.CustomerChainName;
                            mTPRow[2] = currKVP.Value.CustomerLocation.LocationCountry + currKVP.Value.CustomerLocation.LocationPLZ;
                            fixationExclusionProductionProductsTab.Rows.Add(mTPRow);
                        }
                    }

                    fixationExclusionProductionProductsFile = new CSV_File(fixationExclusionProductionProductsFN);
                    fixationExclusionProductionProductsFile.IsHeadLine = true;
                    fixationExclusionProductionProductsFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    fixationExclusionProductionProductsFile.openForWriting(false);
                    fixationExclusionProductionProductsFile.writeData(fixationExclusionProductionProductsTab);
                    fixationExclusionProductionProductsFile.closeDoc();

                    CSV_File fixationExclusionTransportationProductsFile;
                    DataTable fixationExclusionTransportationProductsTab;
                    //mTPRow = new string[3];
                    fixationExclusionTransportationProductsTab = new DataTable();
                    fixationExclusionTransportationProductsTab.Columns.Add("Produkt");
                    fixationExclusionTransportationProductsTab.Columns.Add("Kundenkette");
                    fixationExclusionTransportationProductsTab.Columns.Add("Standort");
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> currKVP in constrSDLProdProcesses)
                    {
                        if (currKVP.Value.IsTransport)
                        {
                            mTPRow[0] = currKVP.Value.ProductName;
                            mTPRow[1] = currKVP.Value.CustomerChainName;
                            mTPRow[2] = currKVP.Value.CustomerLocation.LocationCountry + currKVP.Value.CustomerLocation.LocationPLZ;
                            fixationExclusionTransportationProductsTab.Rows.Add(mTPRow);
                        }
                    }

                    fixationExclusionTransportationProductsFile = new CSV_File(fixationExclusionTransportationProductsFN);
                    fixationExclusionTransportationProductsFile.IsHeadLine = true;
                    fixationExclusionTransportationProductsFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    fixationExclusionTransportationProductsFile.openForWriting(false);
                    fixationExclusionTransportationProductsFile.writeData(fixationExclusionTransportationProductsTab);
                    fixationExclusionTransportationProductsFile.closeDoc();

                    CustomerRelatedProductionProcess currCRPP;
                    //CustomerRelatedPrioritizedProductionProcess currCRPPP;
                    Int32 currConstrNum;
                    Int32 currPConstrNum;
                    currConstrNum = 0;
                    currPConstrNum=0;
                    foreach (MultiDestLocationsConstrainedProductionProcessEntry cMDLPPE in constrMDLProdProcesses)
                    {
                        //if (!cMDLPPE.IsPrioritizedProduction)
                        //{
                        currCRPP = new CustomerRelatedProductionProcess();

                        currCRPP.Id = Convert.ToString(currConstrNum);


                        foreach (ProductionProcessEntry currPPE in cMDLPPE.AllowedProcesses)
                        {
                            //currCRPP.addProcessName(currPPE.Process.Key);
                            currCRPP.addProcessInfo(currPPE.Process.Key, currPPE.IsPrioritizedProcess);
                        }

                        foreach (string currCustLocationId in cMDLPPE.CustomerLocIds)
                        {
                            currCRPP.addCustomerLocationName(currCustLocationId);
                        }

                        pI.addCustomerRelatedProductionProcess(currCRPP);
                        currConstrNum++;
                        /*}
                        else
                        {
                            currCRPPP = new CustomerRelatedPrioritizedProductionProcess();
                            currCRPPP.Id = Convert.ToString(currPConstrNum);

                            foreach (ProductionProcessEntry currPPE in cMDLPPE.AllowedProcesses)
                            {
                                //if (currPPE.IsProcessedEntry)
                                currCRPPP.addProcessInfo(currPPE.Process.Key, true);
                                //else
                                  //  currCRPPP.addProcessInfo(currPPE.Process.Key, false);
                                    //currCRPPP.addProcessName(currPPE.Key);
                            }

                            foreach (string currCustLocationId in cMDLPPE.CustomerLocIds)
                            {
                                currCRPPP.addCustomerLocationName(currCustLocationId);
                            }

                            pI.addCustomerRelatedPrioritizedProductionProcess(currCRPPP);
                            currPConstrNum++;
                        }*/
                    }

                    DataTable processesAddedTab;
                    mTPRow = new string[2];
                    processesAddedTab = new DataTable();
                    processesAddedTab.Columns.Add("ProduktKundenStandort");
                    processesAddedTab.Columns.Add("Prozess");
                    foreach (KeyValuePair<string, HashSet<string>> currPKVP in processesAddedCustProdLoc)
                    {
                        foreach (String currProcessName in currPKVP.Value)
                        {
                            mTPRow[0] = currPKVP.Key;
                            mTPRow[1] = currProcessName;
                        }
                        processesAddedTab.Rows.Add(mTPRow);
                    }

                    processesAddedFile = new CSV_File(processesAddedFN);
                    processesAddedFile.CSVDelimiter = currC.TextInfo.ListSeparator;
                    processesAddedFile.IsHeadLine = true;
                    processesAddedFile.openForWriting(false);
                    processesAddedFile.writeData(processesAddedTab);
                    processesAddedFile.closeDoc();

                    pNCRow = new string[resourceNotConsideredColumnNames.Length];
                    resourcesNotConsideredTab = new DataTable();

                    for (i = 0; i < resourceNotConsideredColumnNames.Length; i++)
                        resourcesNotConsideredTab.Columns.Add(resourceNotConsideredColumnNames[i]);

                    foreach (KeyValuePair<string, ResourceInformationState> rIST in resourcesNotConsidered)
                    {
                        pNCRow[0] = rIST.Key;
                        if (rIST.Value.HasNoLocation)
                            pNCRow[1] = ERROR_INFO_TRUE;
                        else
                            pNCRow[1] = ERROR_INFO_FALSE;


                        if (rIST.Value.HasNoCapacity)
                            pNCRow[2] = ERROR_INFO_TRUE;
                        else
                            pNCRow[2] = ERROR_INFO_FALSE;

                        resourcesNotConsideredTab.Rows.Add(pNCRow);
                    }

                    CustomerProductRelatedTransportationProcess currCRTP;
                    foreach (KeyValuePair<string, SingleDestLocationConstrainedProductionProcessEntry> cSDLPPEKV in constrSDLProdProcesses)
                    {
                        foreach (KeyValuePair<String,TransportationProcessEntry> currTranspEntryKVP in cSDLPPEKV.Value.AllowedTransportationProcesses)
                        {
                            currCRTP = new CustomerProductRelatedTransportationProcess();
                            currCRTP.TransportationProcessName = currTranspEntryKVP.Value.Process.Key;
                            currCRTP.ProductName = cSDLPPEKV.Value.ProductName;
                            pI.addCustomerProductTransportationProcess(currCRTP);
                        }
                        /*
                        foreach (TransportationProcessEntry currTranspEntry in cSDLPPEKV.Value.AllowedTransportationProcesses)
                        {
                            currCRTP = new CustomerProductRelatedTransportationProcess();
                            currCRTP.TransportationProcessName = currTranspEntry.Process.Key;
                            currCRTP.ProductName = cSDLPPEKV.Value.ProductName;
                            pI.addCustomerProductTransportationProcess(currCRTP);
                        }
                         */
                    }

                    foreach (KeyValuePair<String, TransportationProcess> currTranspKV in preliminaryTransportationProcesses)
                    {
                        pI.addCustomerTransportationProcess(currTranspKV.Value);
                    }
                }
                else
                {
                    if (locsLocsMachinesST == usedSTSched.FirstSubTaskWithException && locsLocsMachinesST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw locsLocsMachinesST.Exc;

                    if (transportStartSubstitutionsST == usedSTSched.FirstSubTaskWithException && locsLocsMachinesST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw transportStartSubstitutionsST.Exc;

                    if (demandDataST == usedSTSched.FirstSubTaskWithException && demandDataST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw demandDataST.Exc;

                    if (shiftDataST == usedSTSched.FirstSubTaskWithException && shiftDataST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw shiftDataST.Exc;

                    if (tonnageDataST == usedSTSched.FirstSubTaskWithException && tonnageDataST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw tonnageDataST.Exc;

                    if (baseDataAndWorkingPlansST == usedSTSched.FirstSubTaskWithException && baseDataAndWorkingPlansST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw baseDataAndWorkingPlansST.Exc;

                    if (tpNEEuropeST == usedSTSched.FirstSubTaskWithException && tpNEEuropeST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw tpNEEuropeST.Exc;

                    if (tpSWEuropeST == usedSTSched.FirstSubTaskWithException && tpSWEuropeST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw tpSWEuropeST.Exc;

                    if (whseSWEuropeST == usedSTSched.FirstSubTaskWithException && whseSWEuropeST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw whseSWEuropeST.Exc;

                    if (transportDataST == usedSTSched.FirstSubTaskWithException && transportDataST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw transportDataST.Exc;

                    if (productionProcessesST == usedSTSched.FirstSubTaskWithException && productionProcessesST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw productionProcessesST.Exc;

                    if (transportProcessesST == usedSTSched.FirstSubTaskWithException && transportProcessesST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw transportProcessesST.Exc;

                    if (customerDemandsST == usedSTSched.FirstSubTaskWithException && customerDemandsST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw customerDemandsST.Exc;

                    if (basketsST == usedSTSched.FirstSubTaskWithException && basketsST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw basketsST.Exc;

                    if (interplantTransportsST == usedSTSched.FirstSubTaskWithException && interplantTransportsST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw interplantTransportsST.Exc;

                    if (initialStockST == usedSTSched.FirstSubTaskWithException && initialStockST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw initialStockST.Exc;

                    if (transportStartSubstitutionsST == usedSTSched.FirstSubTaskWithException && transportStartSubstitutionsST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw transportStartSubstitutionsST.Exc;

                    if (specialFixationST == usedSTSched.FirstSubTaskWithException && specialFixationST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw specialFixationST.Exc;

                    if (specialExclusionST == usedSTSched.FirstSubTaskWithException && specialExclusionST.CurrentState == SubTask.SubTaskState.FINISHED_ERROR)
                        throw specialExclusionST.Exc;
                }
            }
            catch (Optimizer_Exceptions.OptimizerException e)
            {
                GC.Collect();
                throw e;
            }
            catch (Exception e)
            {
                GC.Collect();
                throw new Optimizer_Exceptions.OptimizerException(OptimizerException.OptimizerExceptionType.UnknownError, e.Message);
            }

            totalTasksCompleted++;
            if (usedProgressLogger != null)
                usedProgressLogger.progressCallBack((int)((double)totalTasksCompleted / ((double)totalNumberOfTasks) * 100));

            GC.Collect();

            return pI;
        }

        public void outputMessage(LoggerMsgType type, string msg)
        {
            loggers.outputMessage(type, msg, 0);
        }

        protected void getLocationsAndLocsMachines(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getLocationsAndLocsMachines(tPE, ref wPlantInfos,
                                                                                    ref wPlantLocations,
                                                                                    ref wResources,
                                                                                    resourcesNotConsidered,
                                                                                    loggers,
                                                                                    currC);
        }

        public SNP_DataProvider()
        {
            currC = CultureInfo.CurrentCulture;
            schedMut = new Mutex();
            stopSignalMut = new Mutex();
        }

        public void setLoggers(LoggerCollection lC)
        {
            loggers = lC;
        }

        protected void getInitialStocks(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getInitialStocks(tPE,
                              wPlantLocations,
                              preliminaryProducts,
                              ref preliminaryInitialStocks,
                              loggers,
                             currC
                              );
        }



        protected void getTransportStartSubstitutions(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getTransportStartSubstitutions(tPE,
                                           ref transportStartSubstitions,
                                           loggers);
        }

        //1. Transports with cost
        protected void getTransportData(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getTransportData(tPE,
                                                       transportCustomerCustomerChainMatchings,
                                                       ref wCustomerLocations,
                                                       ref productsForCustomerLocation,
                                                       ref toursForCustomer,
                                                       ref tours,
                                                       ref locationDistributionPerCustomerProduct,
                                                       ref locationDistributionPerCustomer,
                                                       ref warehouseLocsSWEurope,
                                                       ref icTours,
                                                       wPlantLocations,
                                                       wPlantInfos,
                                                       transportStartSubstitions,
                                                       pI,
                                                       currC,
                                                       loggers,
                                                       isUseGivenCustomerSalesToCustomerTransportRelations
             );
        }

        protected void getDemandData(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getDemandData(tPE,
                ref rawDemands,
                ref preliminaryProducts,
                ref historicalProductionProcesses,
                ref customerProductRelatedHistoricalProductionProcesses,
                ref preliminaryHistoricalProductions,
                 ref preliminaryHistoricalProductionProcessesCustomerCountryProduct,
                 ref defaultCustomerWarehousesForCustomerCountryProduct,
                //ref articlesWithDemandsOfCustomer,
                ref customersWithDemands,
                pI,
                currC,
                firstMonth,
                DEFAULT_KG_PER_PALLET,
                loggers,
                isConsiderCountryInfoOfDemands,
                wPlantLocations
            );
        }

        protected void getTonnageData(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getTonnageData(tPE,
                          ref tonnageProductionProcesses,
                          preliminaryResources,
                        //  articlesWithTonnages,
                          loggers
                        );
        }

        protected void getBaseDataAndWorkingPlans(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getBaseDataAndWorkingPlans(tPE,
                                      ref preliminaryProductionProcesses,
                                      ref productsWithProcesses,
                                      loggers,
                                      wResources,
                                      preliminaryResources,
                                      tonnageProductionProcesses,
                                      resourcesNotConsidered,
                                      processesNotConsidered,
                                      preliminaryProducts,
                                      usedPreliminaryResources,
                                      historicalProductionProcesses,
                                      currC,
                                      isUseHistoricalProcessIfNoTonnages,
                                      isUseTotalCostPerShift
                                     );
        }

        protected void getShiftData(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getShiftData(tPE,
                       ref preliminaryResources,
                       loggers,
                       wPlantLocations,
                       wResources,
                       resourcesNotConsidered,
                       pI,
                       currC,
                       ref usedPreliminaryResources
                     );
        }

        
        protected void loadSpecialFixations(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getSpecialFixations(tPE, ref specialFixations, loggers, currC);
        }

        protected void loadSpecialExclusions(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getSpecialExclusions(tPE, ref specialExclusions, loggers, currC);
        }

        protected void readTransportsNE(EmptyTaskProcEnvironment tPE)
        {
            usedDL.readTransportsNE(
                             tPE,
                             loggers
                             );
        }

        protected void readTransportsSW(EmptyTaskProcEnvironment tPE)
        {
            usedDL.readTransportsSW(tPE, loggers);
        }

        protected void readWHSESWEurope(EmptyTaskProcEnvironment tPE)
        {
            usedDL.readWHSESWEurope(tPE, loggers);
        }

        Dictionary<string, HashSet<string>> productDemandLocations;
        HashSet<string> artificialTransportProcesses;
        Dictionary<string, TransportationProcess> preliminaryTransportationProcesses;
        Dictionary<String, HashSet<String>> usedSalesplanEntriesForDemand;
        protected void prepareCustomerDemands(EmptyTaskProcEnvironment tPE)
        {
            string customerProductKey;
            ArticleLocationDistribution currCustomerProductLocationDistribution;
            DemandValue currCustomerDemand;
            WEPA_Location currCustomerLocationInfo;
            bool isExistCustomerLocationsInDemandCountry;
            HashSet<string> currProductsForCustomer;
            string currDemandLocName;
            string currDemandProductName;
            string currProdLoc;
            RawTransportProcess currRawTransProcess;
            HashSet<string> nPFCL;
            double currDemandValue;
            string currDemandPeriodName;
            bool isTransportAddedForCustomerDemand;
            Product currDemandProduct;
            CustomerLocation currCustomerLocation;
            PlantLocation currPlantLocation;
            string currTranspProcName;
            TransportationProcess finalTransProcess;
            double totalNumberPerPallets;
            double totalPotentialPallets = 0, totalPotentialPalletsMP = 0, totalPotentialPalletsP = 0, totalPalletsCD = 0;
            string msg;
            bool hasProductionProcess, hasTransport;
            ProductInformationState currPIS;
            double remainingDemand;
            string currPID;
            IndexedItem<DemandValue> iD;
            IndexedItem<CustomerLocation> lI;
            string currFinalDemandLocName;
            HashSet<string> currProductDemandLocations;
            relevantProductsForCustomer = new Dictionary<string, HashSet<string>>();
            preliminaryTransportationProcesses = new Dictionary<string, TransportationProcess>();
            productDemandLocations = new Dictionary<string, HashSet<string>>();
            msg = "Daten aufbereiten...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
            artificialTransportProcesses = new HashSet<string>();
            usedSalesplanEntriesForDemand = new Dictionary<string, HashSet<string>>();

            //Derive demands related to customer locations
            foreach (KeyValuePair<string, RawDemand> dE in rawDemands)
            {
                if (tPE.StopSignal)
                    return;
                hasProductionProcess = hasTransport = false;
                
                if (productsWithProcesses.Contains(dE.Value.ProductName))
                {
                    customerProductKey = dE.Value.CustomerName + dE.Value.ProductName;

                    //If possible use product specific transports to customer locations
                    if (!locationDistributionPerCustomerProduct.ContainsKey(customerProductKey))
                    {
                        if (!locationDistributionPerCustomer.ContainsKey(dE.Value.CustomerName))
                            currCustomerProductLocationDistribution = null;
                        else
                            currCustomerProductLocationDistribution = locationDistributionPerCustomer[dE.Value.CustomerName];
                    }
                    else
                        currCustomerProductLocationDistribution = locationDistributionPerCustomerProduct[customerProductKey];

                    if (currCustomerProductLocationDistribution != null)
                    {
                        hasProductionProcess = true;
                        isExistCustomerLocationsInDemandCountry = false;
                        currCustomerLocationInfo = null;

                        if (isConsiderCountryInfoOfDemands)
                        {
                            //Check whether there are transports to locations in the country given in the sales plan
                            foreach (KeyValuePair<string, double> dE2 in currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation)
                            {
                                currCustomerLocationInfo = wCustomerLocations[dE2.Key];

                                if (currCustomerLocationInfo.LocationCountry == dE.Value.CountryName)
                                {
                                    isExistCustomerLocationsInDemandCountry = true;
                                    break;
                                }
                            }
                        }

                        //Add product to the set of products for the respective customer
                        //this set is needed to extract the relevant products in the baskets for the customer
                        if (!relevantProductsForCustomer.ContainsKey(dE.Value.CustomerName))
                        {
                            currProductsForCustomer = new HashSet<string>();
                            relevantProductsForCustomer.Add(dE.Value.CustomerName, currProductsForCustomer);
                        }
                        else
                            currProductsForCustomer = relevantProductsForCustomer[dE.Value.CustomerName];

                        if (!currProductsForCustomer.Contains(dE.Value.ProductName))
                            currProductsForCustomer.Add(dE.Value.ProductName);

                        totalNumberPerPallets = 0;
                        totalPotentialPallets = 0;
                        foreach (KeyValuePair<string, double> dE2 in currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation)
                        {
                            currCustomerLocationInfo = wCustomerLocations[dE2.Key];
                           
                            //currFinalDemandLocName = buildDemandLocIdentifier(dE.Value.CustomerName, buildLocationIdentifier(currCustomerLocationInfo.LocationCountry, currCustomerLocationInfo.LocationPLZ));
                            currDemandLocName = dE.Value.CustomerName + dE2.Key;
                            
                            currTransportProcessesToDestLocation = transportProcessesToLocation[currDemandLocName];
                            totalPotentialPallets += dE2.Value;
                            foreach (string currTranspName in currTransportProcessesToDestLocation)
                            {
                                currRawTransProcess = transportProcesses[currTranspName];
                                currProdLoc = dE.Value.ProductName + currRawTransProcess.SourceLocId;
                                //Here we calculate the total number of pallets
                                //Please note: if there are no transports for the country given in the sales plan: we take all transports for the customer product
                                //if transports specific for the customer product are available; otherwise we take all transports for the customer
                                if (
                                    (
                                        (isExistCustomerLocationsInDemandCountry && currCustomerLocationInfo.LocationCountry == dE.Value.CountryName) ||
                                        (!isExistCustomerLocationsInDemandCountry)
                                     ) &&
                                    (
                                    //If feasibility without interplant transports should be guaranteed we make sure that the product can be actually be produced at the start location
                                    //otherwise it is sufficient to check whether the start location is among the plant locations
                                        (productLocation.Contains(currProdLoc) && isWarrantFeasiblityWithoutInterplantTransports) ||
                                         (
                                            !isWarrantFeasiblityWithoutInterplantTransports &&
                                                    wPlantInfos.ContainsKey(currRawTransProcess.SourceLocId)
                                         )
                                     )
                                   )
                                {
                                    hasTransport = true;
                                    totalNumberPerPallets += dE2.Value;
                                    break;
                                }
                            }
                        }

                        if (totalNumberPerPallets == 0)
                        {
                            totalPotentialPalletsMP += dE.Value.Val;
                        }
                        else
                            totalPotentialPalletsP += dE.Value.Val;

                        remainingDemand = dE.Value.Val;

                        foreach (KeyValuePair<string, double> dE2 in currCustomerProductLocationDistribution.NumberPalletsTransportedPerLocation)
                        {
                            currCustomerLocationInfo = wCustomerLocations[dE2.Key];
                            currFinalDemandLocName = buildDemandLocIdentifier(dE.Value.CustomerName, buildLocationIdentifier(currCustomerLocationInfo.LocationCountry, currCustomerLocationInfo.LocationPLZ));
                            currDemandLocName = dE.Value.CustomerName + dE2.Key;
                            currDemandProductName = dE.Value.ProductName;
                            //Here we calculate the demand for the specific destination location as the fraction of the number of pallets in the historical transports from the 
                            //total number of pallets in the historical transports
                            currDemandValue = Math.Ceiling(dE.Value.Val * (dE2.Value / totalNumberPerPallets));
                            
                            currDemandPeriodName = dE.Value.PeriodName;
                            //currDemandCustomerName = "";

                            currTransportProcessesToDestLocation = transportProcessesToLocation[currDemandLocName];

                            isTransportAddedForCustomerDemand = false;
                            if (currDemandValue > remainingDemand)
                                currDemandValue = remainingDemand;
                            


                            if (currDemandValue > 0)
                            {
                                //Please note: if there are no transports for the country given in the sales plan: we take all transports for the customer product
                                //if transports specific for the customer product are available; otherwise we take all transports for the customer

                                foreach (string currTranspName in currTransportProcessesToDestLocation)
                                {
                                    currRawTransProcess = transportProcesses[currTranspName];
                                    currProdLoc = dE.Value.ProductName + currRawTransProcess.SourceLocId;
                                    
                                    if (
                                        (
                                            (
                                             isExistCustomerLocationsInDemandCountry && 
                                             currCustomerLocationInfo.LocationCountry == dE.Value.CountryName
                                             ) 
                                             ||
                                            (!isExistCustomerLocationsInDemandCountry)
                                        ) &&
                                        (
                                        //If feasibility without interplant transports should be guaranteed we make sure that the product can be actually be produced at the start location
                                        //otherwise it is sufficient to check whether the start location is among the plant locations
                                            (productLocation.Contains(currProdLoc) && isWarrantFeasiblityWithoutInterplantTransports) 
                                            ||
                                            (
                                                !isWarrantFeasiblityWithoutInterplantTransports && wPlantInfos.ContainsKey(currRawTransProcess.SourceLocId)
                                            )
                                        )
                                      )
                                    {
                                        //Add product if not yet happened to the set of products in the problem instance
                                        if (pI.getProduct(currDemandProductName) == null)
                                        {
                                            currDemandProduct = preliminaryProducts[currDemandProductName];
                                            pI.addProduct(currDemandProduct);
                                        }
                                      

                                        //Add customer location if not yet happened to the set of customer locations in the problem instance
                                        lI = pI.getCustomerLocation(currFinalDemandLocName);//(currDemandLocName);

                                        if (lI == null)
                                        {
                                            currCustomerLocation = new CustomerLocation();

                                            currCustomerLocation.LocationName = currFinalDemandLocName;//currDemandLocName;
                                            pI.addCustomerLocation(currCustomerLocation);
                                        }

                                        //Add customer transport if not yet happened to the set of customer transports in the problem instance
                                        currTranspProcName = currRawTransProcess.SourceLocId + currFinalDemandLocName;//currDemandLocName;

                                        if (!preliminaryTransportationProcesses.ContainsKey(currTranspProcName))
                                        {
                                            //Add plant location if not yet happened to the set of plant locations in the problem instance
                                            if (pI.getPlantLocation(currRawTransProcess.SourceLocId) == null)
                                            {
                                                currPlantLocation = new PlantLocation();
                                                currPlantLocation.LocationName = currRawTransProcess.SourceLocId;
                                                pI.addPlantLocation(currPlantLocation);
                                            }

                                            finalTransProcess = new TransportationProcess();
                                            finalTransProcess.StartLocation = currRawTransProcess.SourceLocId;
                                            finalTransProcess.DestLocation = currFinalDemandLocName;//currDemandLocName;
                                            finalTransProcess.CostPerTruck = currRawTransProcess.AvgCostPerTruck;

                                            if (//!isAddArtificialCustomerTransports && 
                                                currRawTransProcess.isArtificial)
                                                artificialTransportProcesses.Add(finalTransProcess.Key);
                                           
                                            preliminaryTransportationProcesses.Add(finalTransProcess.Key, finalTransProcess);
                                        }
                                        else
                                        {
                                            //finalTransProcess = tI.item;
                                            finalTransProcess = preliminaryTransportationProcesses[currTranspProcName];
                                        }
                                        currTranspProcName = finalTransProcess.StartLocation + finalTransProcess.DestLocation;
                                        isTransportAddedForCustomerDemand = true;

                                    }
                                }

                                if (isTransportAddedForCustomerDemand)
                                {
                                    //if customer transports have been successfully added to the problem instance
                                    //the demand for the customer location can be added as well if necessary
                                    //Please note: if we calculate the demand for the customer product at a customer location
                                    //we ignore the fact whether transports are actually feasible in order to obtain a more realistic estimate for demand distribution
                                    //Transports may be not feasible as the product may not be produced at the start location which is forbidden in case that
                                    //feasibility without interplant transports is required.
                                    string prodLocKey;
                                    string customerProductCountryKey;


                                    customerProductCountryKey = customerProductKey;
                                    if (isConsiderCountryInfoOfDemands)
                                        customerProductCountryKey += dE.Value.CountryName;
                                    HashSet<String> salesPlanDemandEntries;

                                    prodLocKey = currDemandProductName + currFinalDemandLocName;
                                    if (!usedSalesplanEntriesForDemand.ContainsKey(prodLocKey))// + currDemandCustomerName))
                                    {
                                        salesPlanDemandEntries = new HashSet<string>();
                                        usedSalesplanEntriesForDemand.Add(prodLocKey, salesPlanDemandEntries);
                                    }
                                    else
                                        salesPlanDemandEntries = usedSalesplanEntriesForDemand[prodLocKey];

                                    if (!salesPlanDemandEntries.Contains(customerProductCountryKey))
                                        salesPlanDemandEntries.Add(customerProductCountryKey);

                                    remainingDemand -= currDemandValue;

                                    iD = pI.getDemand(currDemandProductName + currFinalDemandLocName + currDemandPeriodName);// + currDemandCustomerName);//pI.getDemand(currDemandProductName + currDemandLocName + currDemandPeriodName + currDemandCustomerName);

                                    if (iD == null)
                                    {
                                        if (!productsForCustomerLocation.ContainsKey(currDemandLocName))
                                        {
                                            nPFCL = new HashSet<string>();
                                            productsForCustomerLocation.Add(currDemandLocName, nPFCL);
                                        }
                                        else
                                            nPFCL = productsForCustomerLocation[currDemandLocName];

                                        if (!nPFCL.Contains(currDemandProductName))
                                            nPFCL.Add(currDemandProductName);

                                        currCustomerDemand = new DemandValue();
                                        currCustomerDemand.ProductName = currDemandProductName;
                                        currCustomerDemand.LocationName = currFinalDemandLocName;//currDemandLocName;
                                        currCustomerDemand.PeriodName = currDemandPeriodName;
                                        currCustomerDemand.Val = 0;

                                        pI.addDemand(currCustomerDemand);
                                    }
                                    else
                                        currCustomerDemand = iD.item;
                            
                                    currCustomerDemand.Val += currDemandValue;
                                    totalPalletsCD += currDemandValue;
                                   
                                    if (!productDemandLocations .ContainsKey (currDemandProductName ))
                                    {
                                        currProductDemandLocations=new HashSet<string> ();
                                        productDemandLocations .Add(currDemandProductName,currProductDemandLocations);
                                    }
                                    else
                                       currProductDemandLocations =productDemandLocations[currDemandProductName ];

                                    if (!currProductDemandLocations.Contains (currFinalDemandLocName))
                                        currProductDemandLocations.Add (currFinalDemandLocName );
                                }
                            }
                        }
                    }
                }
                else
                {
                    hasProductionProcess = false;
                }

                //If a product either has no production process or no transport
                //it is ignored in the optimization and thus is added to the set of products not considered.
                if (!hasProductionProcess || !hasTransport)
                {
                    currPID = dE.Value.ProductName + dE.Value.CustomerName + dE.Value.CountryName;
                    if (productsNotConsidered.ContainsKey(currPID))
                    {
                        currPIS = productsNotConsidered[currPID];
                    }
                    else
                    {
                        currPIS = new ProductInformationState();
                        currPIS.ProductName = dE.Value.ProductName;
                        currPIS.CustomerName = dE.Value.CustomerName;
                        currPIS.CountryName = dE.Value.CountryName;
                        currPIS.HasNoProductionProcess = currPIS.HasNoTranportationProcess = false;
                        productsNotConsidered.Add(currPID, currPIS);
                    }

                    currPIS.HasNoProductionProcess = !hasProductionProcess;
                    currPIS.HasNoTranportationProcess = !hasTransport;
                }
            }
            
            msg = "Daten fertig.";// +"Zeit:" + sw.Elapsed; ;
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        protected int compareBaskets(HashSet<string> b1, HashSet<string> b2)
        {
            if (b1.Count != b2.Count)
                return 0;

            foreach (string p in b1)
            {
                if (!b2.Contains(p))
                    return 0;
            }

            return 1;
        }

        protected List<HashSet<string>> pruneBasketCandidateSet(List<HashSet<string>> rawSet)
        {
            int i, j;
            bool isAllContained;
            HashSet<string>[] tempBasketCandidateSet;
            List<HashSet<string>> prunedBasketCandidateSet;
            List<HashSet<string>> prunedBasketCandidateSet2;
            bool isNeeded;

            prunedBasketCandidateSet = new List<HashSet<string>>();
            prunedBasketCandidateSet2 = new List<HashSet<string>>();
            tempBasketCandidateSet = new HashSet<string>[rawSet.Count];
            j = 0;

            foreach (HashSet<string> currBasketCandidate in rawSet)
            {
                HashSet<string> tBasketCandidate = new HashSet<string>();

                foreach (string prod in currBasketCandidate)
                    tBasketCandidate.Add(prod);

                tempBasketCandidateSet[j] = tBasketCandidate;

                j++;
            }

            //Eliminiere Warenkörbe, die vollständige durch kleinere Warenkörbe überdeckt werden
            j = 0;
            foreach (HashSet<string> currBasketCandidate in rawSet)
            {
                i = 0;
                foreach (HashSet<string> currBasketCandidate2 in rawSet)
                {
                    if (i != j)
                    {
                        if (currBasketCandidate.Count < currBasketCandidate2.Count || (currBasketCandidate.Count == currBasketCandidate2.Count && j < i))
                        {
                            isAllContained = true;

                            foreach (string prod in currBasketCandidate)
                            {
                                if (!currBasketCandidate2.Contains(prod))
                                {
                                    isAllContained = false;
                                    break;
                                }
                            }
                            if (isAllContained)
                            {
                                foreach (string prod in currBasketCandidate)
                                {
                                    if (tempBasketCandidateSet[i].Contains(prod))
                                        tempBasketCandidateSet[i].Remove(prod);
                                }
                            }
                        }
                    }
                    i++;
                }
                j++;
            }

            j = 0;
            foreach (HashSet<string> currBasketCandidate in rawSet)
            {
                if (tempBasketCandidateSet[j].Count > 0)
                    prunedBasketCandidateSet.Add(currBasketCandidate);
                j++;
            }

            j = 0;
            //Eliminiere Warenkörbe mit einem Element (atomare Warenkörbe), wenn sie in keine
            //größere Menge eingehen
            foreach (HashSet<string> currBasketCandidate in prunedBasketCandidateSet)
            {
                if (currBasketCandidate.Count == 1)
                {
                    isNeeded = false;
                    i = 0;
                    foreach (HashSet<string> currBasketCandidate2 in prunedBasketCandidateSet)
                    {
                        if (i != j && currBasketCandidate.Count < currBasketCandidate2.Count)
                        {
                            isAllContained = true;

                            foreach (string prod in currBasketCandidate)
                            {
                                if (!currBasketCandidate2.Contains(prod))
                                {
                                    isAllContained = false;
                                    break;
                                }
                            }
                            if (isAllContained)
                            {
                                isNeeded = true;
                                break;
                            }
                        }

                        i++;
                    }
                    if (isNeeded)
                        prunedBasketCandidateSet2.Add(currBasketCandidate);

                }
                else
                    prunedBasketCandidateSet2.Add(currBasketCandidate);
            }

            return prunedBasketCandidateSet2;
        }
        HashSet<string> productsWithBaskets;
        Dictionary<string, BasketSet> preliminaryBasketSets;

        protected void prepareBaskets(EmptyTaskProcEnvironment tPE)
        {
            string msg;
            Dictionary<string, List<HashSet<string>>> basketCandidates = new Dictionary<string, List<HashSet<string>>>();
            HashSet<string> currBasketCandidate;
            List<HashSet<string>> currBasketCandidateSet;
            ArticleLocationDistribution currLocDistr;
            HashSet<string> currToursForCustomer;
            TourData currTour;
            string currCustomerLocationName;
            string currLocName;
            List<HashSet<string>> customerBasketSet;
            HashSet<string> customerBasket;
            WEPA_Location currCLoc;

            preliminaryBasketSets = new Dictionary<string, BasketSet>();
            //************************************************************************************************************************************************************************************************
            //***********************************Hier noch einmal prüfen, ob Kundenstandorte in den Schlüssel in productsWithBaskets mit aufgenommen werden sollen********************************************
            //************************************************************************************************************************************************************************************************
            productsWithBaskets = new HashSet<string>();
            
            msg = "Bearbeite Warenkörbe...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            //Analyse der Transportkombinationen um Warenkörbe zu bestimmen
            foreach (KeyValuePair<string, HashSet<string>> customerProductEntry in relevantProductsForCustomer)
            {
                //Determine for each customer the set of baskets 
                if (tPE.StopSignal)
                    return;
                if (toursForCustomer.ContainsKey(customerProductEntry.Key))
                {
                    currToursForCustomer = toursForCustomer[customerProductEntry.Key];

                    currBasketCandidateSet = new List<HashSet<string>>();
                    
                    //Construct all baskets for the respective customer
                    foreach (string tourName in currToursForCustomer)
                    {
                        currTour = tours[tourName];

                        currBasketCandidate = null;

                        foreach (string productName in customerProductEntry.Value)
                        {
                            if (currTour.ProductsOnTour.Contains(productName))
                            {
                                if (currBasketCandidate == null)
                                    currBasketCandidate = new HashSet<string>();

                                currBasketCandidate.Add(productName);
                            }
                        }

                        if (currBasketCandidate != null)
                        {
                            currBasketCandidateSet.Add(currBasketCandidate);
                        }
                    }
                    //Prune basket candidate set by eliminating dominated and redundant basket sets
                    currBasketCandidateSet = pruneBasketCandidateSet(currBasketCandidateSet);

                    currLocDistr = locationDistributionPerCustomer[customerProductEntry.Key];
                    //break down the set of baskets for the customer to sets of baskets for the customer locations
                    foreach (KeyValuePair<string, double> custLocDistr in currLocDistr.NumberPalletsTransportedPerLocation)
                    {  
                        currCLoc = wCustomerLocations[custLocDistr.Key];
                       
                        currCustomerLocationName = buildDemandLocIdentifier(customerProductEntry.Key, buildLocationIdentifier(currCLoc.LocationCountry, currCLoc.LocationPLZ));// customerProductEntry.Key + buildLocationIdentifier(currCLoc.LocationCountry, currCLoc.LocationPLZ);//custLocDistr.Key;
                        
                        if (pI.getCustomerLocation(currCustomerLocationName) != null)
                        {
                            //Consider only existing customer locations (where valid transports exist)
                            currLocName = customerProductEntry.Key + custLocDistr.Key;

                            customerBasketSet = new List<HashSet<string>>();

                            foreach (HashSet<string> currBasket in currBasketCandidateSet)
                            {   
                                customerBasket = new HashSet<string>();

                                foreach (string prodName in currBasket)
                                {
                                    //Add the only to the basket if it is in the set of products with demands 
                                    //for the respective customer location
                                    if (productsForCustomerLocation[currLocName].Contains(prodName))
                                        customerBasket.Add(prodName);
                                }

                                customerBasketSet.Add(customerBasket);
                            }

                            customerBasketSet = pruneBasketCandidateSet(customerBasketSet);

                            if (customerBasketSet.Count > 0)
                            {
                                BasketSet nBaskets = new BasketSet();
                                nBaskets.LocationName = currCustomerLocationName;//currLocName;

                                foreach (HashSet<string> currBasket in customerBasketSet)
                                {
                                    foreach (string pName in currBasket)
                                    {
                          //************************************************************************************************************************************************************************************************
                                        //***********************************Hier noch einmal prüfen, ob Kundenstandorte in den Schlüssel in productsWithBaskets mit aufgenommen werden sollen********************************************
                                        //************************************************************************************************************************************************************************************************
                                       // if (!productsWithBaskets.Contains(currPKey))
                                         //   productsWithBaskets.Add(currPKey);
                                        if (!productsWithBaskets.Contains(pName))
                                            productsWithBaskets.Add(pName);
                                    }
                                    nBaskets.addBasket(currBasket);
                                }

                                //pI.addBasketSet(nBaskets);
                                preliminaryBasketSets.Add(nBaskets.Key, nBaskets);
                            }
                        }
                    }
                }
            }
            //sw.Stop();
            msg = "Warenkörbe fertig.";// +"Zeit:" + sw.Elapsed;
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        protected void getInterplantTransports(EmptyTaskProcEnvironment tPE)
        {
            usedDL.getInterplantTransports(tPE, wPlantLocations, pI, currC, loggers);
        }
        //protected bool isAddArtificialCustomerTransports = false;
        protected void prepareTransportationProcesses(EmptyTaskProcEnvironment tPE)
        {
            HashSet<string> consideredTours;
            string msg;
            string transpProcKey;
            RawTransportProcess currTransportProcess;
            List<string> currTranspProcToLocation;
            WEPA_Location dummyLoc;
            string currCustomerDummyLocation;
            ArticleLocationDistribution currDummyArtLocDistr;
            Dictionary<string, RawTransportProcess> icTransportProcesses;
            HashSet<string> origAddIcTransports;
            WEPA_PlantInfo currPlInfoStart, currPlInfoDest;
            bool isExistTransportToLocation;
            TransportationProcess currICTransportProcess;
            ProductionProcess currProcess;
            WEPA_Resource currResource;
            WEPA_Location currResourceLocation;
            string locId;
            string custProdKey;
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            msg = "Bearbeite Transportprozesse...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            transportProcesses = new Dictionary<string, RawTransportProcess>();
            transportProcessesToLocation = new Dictionary<string, List<string>>();

            consideredTours = new HashSet<string>();

            foreach (KeyValuePair<string, TourData> dE in tours)
            {
                if (tPE.StopSignal)
                    return;

                //check whether the start location of the transport corresponds to a plant location
                if (wPlantInfos.ContainsKey(dE.Value.SourceLocId))
                {
                    transpProcKey = dE.Value.SourceLocId + dE.Value.Customer + dE.Value.DestLocId;
                    //Add transport process to the set of transport processes
                    if (!transportProcesses.ContainsKey(transpProcKey))
                    {
                        currTransportProcess = new RawTransportProcess();
                        currTransportProcess.NumberOfHistoricalTrucks = 0;
                        currTransportProcess.AvgCostPerTruck = 0;
                        currTransportProcess.SourceLocId = dE.Value.SourceLocId;
                        currTransportProcess.DestLocId = dE.Value.Customer + dE.Value.DestLocId;
                        currTransportProcess.isArtificial = false;

                        transportProcesses.Add(transpProcKey, currTransportProcess);

                        if (!transportProcessesToLocation.ContainsKey(dE.Value.Customer + dE.Value.DestLocId))
                        {
                            //Add transport process to the transport processs related to a customer location
                            currTranspProcToLocation = new List<string>();
                            transportProcessesToLocation.Add(dE.Value.Customer + dE.Value.DestLocId, currTranspProcToLocation);
                        }
                        else
                            currTranspProcToLocation = transportProcessesToLocation[dE.Value.Customer + dE.Value.DestLocId];

                        currTranspProcToLocation.Add(transpProcKey);
                    }
                    else
                        currTransportProcess = transportProcesses[transpProcKey];

                    if (!isIgnoreTrucksPerTour)
                    {
                        //Calculation of the number of trucks for each tour respectively
                        currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
                        currTransportProcess.AvgCostPerTruck += dE.Value.TourPrice;
                    }
                    else
                    {
                        //Calculation of the overall number of trucks
                        currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets);
                        currTransportProcess.AvgCostPerTruck += dE.Value.TourPrice;
                    }
                }
            }

            foreach (KeyValuePair<string, TourData> dE in tours)
            {
                if (tPE.StopSignal)
                    return;
                if (!wPlantInfos.ContainsKey(dE.Value.SourceLocId))
                {
                    //if Source-Location does not corresponds to a plant location the transport is replaced by transports from all plants to the customer location at zero cost
                    if (isWarrantFeasiblityWithoutInterplantTransports)
                    {
                        //If feasibility should be warranted on absence of interplant transports
                        //add for every customer location and plant location an artificial transport when it does not exist yet
                        foreach (KeyValuePair<string, WEPA_PlantInfo> wPLI in wPlantInfos)
                        {
                            transpProcKey = wPLI.Key + dE.Value.Customer + dE.Value.DestLocId;

                            if (!transportProcesses.ContainsKey(transpProcKey))
                            {
                                currTransportProcess = new RawTransportProcess();
                                currTransportProcess.NumberOfHistoricalTrucks = 1;
                                currTransportProcess.AvgCostPerTruck = 0;
                                currTransportProcess.SourceLocId = wPLI.Key;
                                currTransportProcess.DestLocId = dE.Value.Customer + dE.Value.DestLocId;
                                currTransportProcess.isArtificial = true;
                                transportProcesses.Add(transpProcKey, currTransportProcess);

                                if (!transportProcessesToLocation.ContainsKey(dE.Value.Customer + dE.Value.DestLocId))
                                {
                                    currTranspProcToLocation = new List<string>();
                                    transportProcessesToLocation.Add(dE.Value.Customer + dE.Value.DestLocId, currTranspProcToLocation);
                                }
                                else
                                    currTranspProcToLocation = transportProcessesToLocation[dE.Value.Customer + dE.Value.DestLocId];

                                currTranspProcToLocation.Add(transpProcKey);

                                if (!isIgnoreTrucksPerTour)
                                    currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
                                else
                                    currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets);
                            }
                        }
                    }
                    else
                    {
                        isExistTransportToLocation = false;
                        
                        foreach (KeyValuePair<string, WEPA_PlantInfo> wPLI in wPlantInfos)
                        {
                            transpProcKey = wPLI.Key + dE.Value.Customer + dE.Value.DestLocId;

                            if (transportProcesses.ContainsKey(transpProcKey))
                            {
                                isExistTransportToLocation = true;
                                break;
                            }
                        }

                        if (!isExistTransportToLocation && isCheckCountryMissingCustomerTransportConnections)
                        {
                            //If interplant transports are allowed add only artificial transports if there does not exist a single transport to the customer location
                            foreach (KeyValuePair<string, WEPA_PlantInfo> wPLI in wPlantInfos)
                            {
                                transpProcKey = wPLI.Key + dE.Value.Customer + dE.Value.DestLocId;
                                WEPA_Location currWPL = wPlantLocations[wPLI.Value.LocationName];
                                WEPA_Location currCL = wCustomerLocations[dE.Value.DestLocId];
                                if (currWPL.LocationCountry == currCL.LocationCountry)
                                {
                                    if (!transportProcesses.ContainsKey(transpProcKey))
                                    {
                                        currTransportProcess = new RawTransportProcess();
                                        currTransportProcess.NumberOfHistoricalTrucks = 1;
                                        currTransportProcess.AvgCostPerTruck = 0;
                                        currTransportProcess.SourceLocId = wPLI.Key;//dE.Value.sourceLocId;
                                        currTransportProcess.DestLocId = dE.Value.Customer + dE.Value.DestLocId;
                                        currTransportProcess.isArtificial = true;
                                        transportProcesses.Add(transpProcKey, currTransportProcess);

                                        if (!transportProcessesToLocation.ContainsKey(dE.Value.Customer + dE.Value.DestLocId))
                                        {
                                            currTranspProcToLocation = new List<string>();
                                            transportProcessesToLocation.Add(dE.Value.Customer + dE.Value.DestLocId, currTranspProcToLocation);
                                        }
                                        else
                                            currTranspProcToLocation = transportProcessesToLocation[dE.Value.Customer + dE.Value.DestLocId];

                                        currTranspProcToLocation.Add(transpProcKey);
                                        currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
                                        isExistTransportToLocation = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!isExistTransportToLocation)
                        {
                            //If interplant transports are allowed add only artificial transports if there does not exist a single transport to the customer location
                            foreach (KeyValuePair<string, WEPA_PlantInfo> wPLI in wPlantInfos)
                            {
                                transpProcKey = wPLI.Key + dE.Value.Customer + dE.Value.DestLocId;

                                if (!transportProcesses.ContainsKey(transpProcKey))
                                {
                                    currTransportProcess = new RawTransportProcess();
                                    currTransportProcess.NumberOfHistoricalTrucks = 1;
                                    currTransportProcess.AvgCostPerTruck = 0;
                                    currTransportProcess.SourceLocId = wPLI.Key;//dE.Value.sourceLocId;
                                    currTransportProcess.DestLocId = dE.Value.Customer + dE.Value.DestLocId;
                                    currTransportProcess.isArtificial = true;
                                    transportProcesses.Add(transpProcKey, currTransportProcess);

                                    if (!transportProcessesToLocation.ContainsKey(dE.Value.Customer + dE.Value.DestLocId))
                                    {
                                        currTranspProcToLocation = new List<string>();
                                        transportProcessesToLocation.Add(dE.Value.Customer + dE.Value.DestLocId, currTranspProcToLocation);
                                    }
                                    else
                                        currTranspProcToLocation = transportProcessesToLocation[dE.Value.Customer + dE.Value.DestLocId];

                                    currTranspProcToLocation.Add(transpProcKey);
                                    currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
                                    isExistTransportToLocation = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            //}
            foreach (KeyValuePair<string, RawTransportProcess> dE in transportProcesses)
            {
                //Complete the calculation of the average cost per truck
                if (tPE.StopSignal)
                    return;
                if (isIgnoreTrucksPerTour)
                    dE.Value.AvgCostPerTruck /= (dE.Value.NumberOfHistoricalTrucks / (double)NUMBER_PALLETS_PER_TRUCK);
                else
                    dE.Value.AvgCostPerTruck /= dE.Value.NumberOfHistoricalTrucks;

                if (dE.Value.AvgCostPerTruck < THRESHOLD_ACCEPTANCE_AVG_TRANSPORT_COST && dE.Value.AvgCostPerTruck > 0)
                    dE.Value.AvgCostPerTruck = 0;
            }

            icTransportProcesses = new Dictionary<string, RawTransportProcess>();

            origAddIcTransports = new HashSet<string>();

            foreach (KeyValuePair<string, TourData> dE in icTours)
            {
                //If there are missing interplant transports from the ic-cost matrix
                //add the missing transports from the historical transports
                if (tPE.StopSignal)
                    return;
                transpProcKey = dE.Value.SourceLocId + dE.Value.DestLocId;

                if (!icTransportProcesses.ContainsKey(transpProcKey))
                {
                    currTransportProcess = new RawTransportProcess();
                    currTransportProcess.NumberOfHistoricalTrucks = 0;
                    currTransportProcess.AvgCostPerTruck = 0;
                    currTransportProcess.SourceLocId = dE.Value.SourceLocId;
                    currTransportProcess.DestLocId = dE.Value.DestLocId;
                    currTransportProcess.isArtificial = false;
                    icTransportProcesses.Add(transpProcKey, currTransportProcess);
                    origAddIcTransports.Add(transpProcKey);
                }
                else
                    currTransportProcess = icTransportProcesses[transpProcKey];

                if (!isIgnoreTrucksPerTour)
                {
                    currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
                    currTransportProcess.AvgCostPerTruck += dE.Value.TourPrice;
                }
                else
                {
                    currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets);
                    currTransportProcess.AvgCostPerTruck += dE.Value.TourPrice;
                }
            }

            if (isAddSymmetricICTransportsIfNotExisting)
            {
                //Add symmetric ic transports if there are still missing interplant transports
                foreach (KeyValuePair<string, TourData> dE in icTours)
                {
                    transpProcKey = dE.Value.DestLocId + dE.Value.SourceLocId;
                    if (!origAddIcTransports.Contains(transpProcKey))
                    {
                        if (!icTransportProcesses.ContainsKey(transpProcKey))
                        {
                            currTransportProcess = new RawTransportProcess();
                            currTransportProcess.NumberOfHistoricalTrucks = 0;
                            currTransportProcess.AvgCostPerTruck = 0;
                            currTransportProcess.DestLocId = dE.Value.SourceLocId;
                            currTransportProcess.SourceLocId = dE.Value.DestLocId;
                            currTransportProcess.isArtificial = false;
                            icTransportProcesses.Add(transpProcKey, currTransportProcess);
                        }
                        else
                            currTransportProcess = icTransportProcesses[transpProcKey];

                        if (!isIgnoreTrucksPerTour)
                        {
                            currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets / ((double)NUMBER_PALLETS_PER_TRUCK));
                            currTransportProcess.AvgCostPerTruck += dE.Value.TourPrice;
                        }
                        else
                        {
                            currTransportProcess.NumberOfHistoricalTrucks += (int)Math.Ceiling(dE.Value.TourNumberPallets);
                            currTransportProcess.AvgCostPerTruck += dE.Value.TourPrice;
                        }
                    }
                }
            }

            foreach (KeyValuePair<string, RawTransportProcess> dE in icTransportProcesses)
            {
                if (tPE.StopSignal)
                    return;
                if (isIgnoreTrucksPerTour)
                    dE.Value.AvgCostPerTruck /= (dE.Value.NumberOfHistoricalTrucks / (double)NUMBER_PALLETS_PER_TRUCK);
                else
                    dE.Value.AvgCostPerTruck /= dE.Value.NumberOfHistoricalTrucks;

                if (dE.Value.AvgCostPerTruck < THRESHOLD_ACCEPTANCE_AVG_TRANSPORT_COST && dE.Value.AvgCostPerTruck > 0)
                    dE.Value.AvgCostPerTruck = 0;

                currICTransportProcess = new TransportationProcess();
                currICTransportProcess.StartLocation = dE.Value.SourceLocId;
                currICTransportProcess.DestLocation = dE.Value.DestLocId;
                currICTransportProcess.CostPerTruck = dE.Value.AvgCostPerTruck;

                currPlInfoStart = wPlantInfos[currICTransportProcess.StartLocation];
                currPlInfoDest = wPlantInfos[currICTransportProcess.DestLocation];

                msg = "Missing interplant transport from " + currPlInfoStart.LocationName + " to " + currPlInfoDest.LocationName + " added with transport cost estimated from historical data.";
                loggers.outputMessage(LoggerMsgType.WARNING_MSG, msg, 0);
                pI.addInterplantTransportationProcess(currICTransportProcess);
            }

            foreach (string customerName in customersWithDemands)
            {  
                if (tPE.StopSignal)
                    return;
                
                if (!locationDistributionPerCustomer.ContainsKey(customerName))
                {
                    if (isAddCustomerProductRelatedTransportsForDummyCustomers)
                    {
                        HashSet<String> customerProductHistProdProcessIds;

                        customerProductHistProdProcessIds = customerProductRelatedHistoricalProductionProcesses[customerName];

                        foreach (String currHistProcessId in customerProductHistProdProcessIds)
                        {
                            if (preliminaryProductionProcesses.ContainsKey(currHistProcessId))//(currProcessId))
                            {
                                currProcess = preliminaryProductionProcesses[currHistProcessId];
                                currResource = wResources[currProcess.ResourceName];
                                currResourceLocation = wPlantLocations[currResource.LocationName];

                                locId = currResourceLocation.LocationCountry + currResourceLocation.LocationPLZ;// +DUMMY_LOCATION_NAME;
                                currCustomerDummyLocation = customerName + locId;//+ locId + DUMMY_LOCATION_NAME;
                                

                                custProdKey = customerName + currProcess.ProductName;
                                if (!locationDistributionPerCustomerProduct.ContainsKey(custProdKey))//(customerProductHistProdProcessKVP.Key))
                                {
                                    currDummyArtLocDistr = new ArticleLocationDistribution();
                                    locationDistributionPerCustomerProduct.Add(custProdKey, currDummyArtLocDistr);//(customerProductHistProdProcessKVP.Key, currDummyArtLocDistr);
                                }
                                else
                                    currDummyArtLocDistr = locationDistributionPerCustomerProduct[custProdKey];//[customerProductHistProdProcessKVP.Key];

                                if (!currDummyArtLocDistr.NumberPalletsTransportedPerLocation.ContainsKey (locId))
                                currDummyArtLocDistr.NumberPalletsTransportedPerLocation.Add(locId , 1); //Eine symbolische Palette, damit Division durch Palettenanzahl funktioniert.

                                if (!wCustomerLocations.ContainsKey(locId ))
                                {
                                    dummyLoc = new WEPA_Location();
                                    dummyLoc.LocationCountry = currResourceLocation.LocationCountry;
                                    dummyLoc.LocationPLZ = currResourceLocation.LocationPLZ;
                                    wCustomerLocations.Add(locId , dummyLoc);
                                }

                                currTransportProcess = new RawTransportProcess();
                                currTransportProcess.NumberOfHistoricalTrucks = 1;
                                currTransportProcess.AvgCostPerTruck = 0;
                                currTransportProcess.SourceLocId = currResourceLocation.LocationCountry + currResourceLocation.LocationPLZ;
                                currTransportProcess.DestLocId = currCustomerDummyLocation;
                                currTransportProcess.isArtificial = false;
                                transpProcKey = currTransportProcess.SourceLocId + currCustomerDummyLocation;

                                if (!transportProcesses.ContainsKey(transpProcKey))
                                {
                                    transportProcesses.Add(transpProcKey, currTransportProcess);

                                    if (!transportProcessesToLocation.ContainsKey(currCustomerDummyLocation))
                                    {
                                        currTranspProcToLocation = new List<string>();
                                        transportProcessesToLocation.Add(currCustomerDummyLocation, currTranspProcToLocation);
                                    }
                                    else
                                        currTranspProcToLocation = transportProcessesToLocation[currCustomerDummyLocation];

                                    currTranspProcToLocation.Add(transpProcKey);
                                }
                            }
                            //}
                        }
                    }
                    else
                    {
                        //Add artificial customer locations if for customers do not exist any locations
                        //obtained from historical transport data
                        currCustomerDummyLocation = customerName + DUMMY_LOCATION_NAME;
                        currDummyArtLocDistr = new ArticleLocationDistribution();
                        currDummyArtLocDistr.NumberPalletsTransportedPerLocation.Add(DUMMY_LOCATION_NAME, 1); //Eine symbolische Palette, damit Division durch Palettenanzahl funktioniert.
                        locationDistributionPerCustomer.Add(customerName, currDummyArtLocDistr);
                        if (!wCustomerLocations.ContainsKey(DUMMY_LOCATION_NAME))
                        {
                            dummyLoc = new WEPA_Location();
                            dummyLoc.LocationCountry = "DUMMY_COUNTRY";
                            dummyLoc.LocationPLZ = "DUMMY_PLZ";
                            wCustomerLocations.Add(DUMMY_LOCATION_NAME, dummyLoc);
                        }
                        foreach (KeyValuePair<string, WEPA_Location> plantInfo in wPlantLocations)
                        {
                            currTransportProcess = new RawTransportProcess();
                            currTransportProcess.NumberOfHistoricalTrucks = 1;
                            currTransportProcess.AvgCostPerTruck = 0;
                            currTransportProcess.SourceLocId = plantInfo.Value.LocationCountry + plantInfo.Value.LocationPLZ;
                            currTransportProcess.DestLocId = currCustomerDummyLocation;
                            currTransportProcess.isArtificial = false;
                            transpProcKey = currTransportProcess.SourceLocId + currCustomerDummyLocation;
                            transportProcesses.Add(transpProcKey, currTransportProcess);

                            if (!transportProcessesToLocation.ContainsKey(currCustomerDummyLocation))
                            {
                                currTranspProcToLocation = new List<string>();
                                transportProcessesToLocation.Add(currCustomerDummyLocation, currTranspProcToLocation);
                            }
                            else
                                currTranspProcToLocation = transportProcessesToLocation[currCustomerDummyLocation];

                            currTranspProcToLocation.Add(transpProcKey);
                        }
                    }
                }
            }
            //sw.Stop();

            msg = "Transportprozesse fertig.";// +"Zeit:" + sw.Elapsed; ;
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }

        Dictionary<string, HashSet<string>> productProductionLocations;
        protected void prepareProductionProcesses(EmptyTaskProcEnvironment tPE)
        {
            string msg;
            Resource currResource;
            string currProductLocation;
         
            HashSet<string> currProdLocations;
         
            msg = "Bearbeite Produktionsprozesse...";
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);

            productLocation = new HashSet<string>();
           
            productProductionLocations = new Dictionary<string, HashSet<string>>();
            foreach (KeyValuePair<string, ProductionProcess> kP in preliminaryProductionProcesses)
            {
                //Determines all product and plant location combinations according to the production processes
                //They are needed to determine the feasible transports from plant locations to customer locations (in prepareCustomerDemands)
                //if feasiblity should be warranted.
                if (tPE.StopSignal)
                    return;

                currResource = preliminaryResources[(string)kP.Value.ResourceName];

                currProductLocation = kP.Value.ProductName + currResource.LocationName;//+ currResourceLocation;

                if (!productProductionLocations.ContainsKey(kP.Value.ProductName))
                {
                    currProdLocations = new HashSet<string>();
                    productProductionLocations.Add(kP.Value.ProductName, currProdLocations);
                }
                else
                {
                    currProdLocations=productProductionLocations[kP.Value.ProductName];
                }

                if (!currProdLocations.Contains(currResource.LocationName))
                    currProdLocations.Add(currResource.LocationName);

                if (!productLocation.Contains(currProductLocation))
                {
                    productLocation.Add(currProductLocation);
                }
            }
            
            msg = "Produktionsprozesse fertig.";// +"Zeit:" + sw.Elapsed;
            loggers.outputMessage(LoggerMsgType.INFO_MSG, msg, 0);
        }
    }
}
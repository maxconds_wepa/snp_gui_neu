﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace SNP_Optimizer
{
    public class SubTask 
    {
        public interface ISubtaskProcEnvironment {
            void setSubTask(SubTask sT);
            void start();
            void stop();
        }

        public interface ISubTaskScheduler
        {
            void completionCallBack(SubTask sender);
            void exceptionCallBack(SubTask sender,Exception e);
        }

        ISubTaskScheduler sched;

        public enum SubTaskState {
                    NOT_STARTED,
                    RUNNING,
                    FINISHED_OK,
                    FINISHED_ERROR
        }

        protected Exception exc;

        protected ISubtaskProcEnvironment stprc;

        protected Thread sThread;

        protected SubTaskState currState;

        protected List<SubTask> predecessors;
        
        protected Mutex stateMut;

        public ISubtaskProcEnvironment Procedure
        {
            get { return stprc; }
            set {
                stprc = value;
                stprc.setSubTask(this);
            }
        }

        public Exception Exc
        {
            get { return exc; }
            set
            {
                exc = value;
            }
        }

        public SubTask(ISubTaskScheduler sch)
        {
            sched = sch;
            predecessors = new List<SubTask>();
            currState = SubTaskState.NOT_STARTED;
            stateMut = new Mutex();
        }

        public void addPredecessor(SubTask p)
        {
            predecessors .Add (p);
        }

        public SubTaskState CurrentState
        {
            get {
                SubTaskState currSt;
                stateMut.WaitOne();
                currSt = currState;
                stateMut.ReleaseMutex();
                return currSt; 
            }
            set {
                stateMut.WaitOne();
                currState = value;
                stateMut.ReleaseMutex();
            }
        }

        public bool IsReadyToStart
        {
            get {
                bool isReady=true;

                foreach (SubTask prd in predecessors )
                {
                    if (prd.CurrentState != SubTaskState.FINISHED_OK)
                        isReady = false;
                }

                return isReady; 
            }
        }
        protected bool isWithExceptionHandling = true;
        protected void threadCore()
        {
            if (isWithExceptionHandling)
            {
                try
                {
                    stprc.start();

                    //Create completion event
                    currState = SubTaskState.FINISHED_OK;
                    sched.completionCallBack(this);
                }
                catch (Exception e)
                {
                    sched.exceptionCallBack(this, e);
                }
            }
            else
            {
                stprc.start();

                //Create completion event
                currState = SubTaskState.FINISHED_OK;
                sched.completionCallBack(this);
            }
        }

        public void startSubTask()
        {
            sThread = new Thread(threadCore);
            currState = SubTaskState.RUNNING;
            sThread.Start();
        }

        public void stopThread()
        {
            if (sThread != null)
                sThread.Abort();
        }

        public void stopSubTask()
        {
            stprc.stop();
        }

        public void waitForCompletion()
        {
            sThread.Join();
        }

        public void waitForReadiness()
        {
            foreach (SubTask prd in predecessors)
            {

            }
        }
    }

    public class SubTaskScheduler : SubTask.ISubTaskScheduler
    {
        protected Mutex mut;

        public interface ISubTaskSchedulerEnvironment
        {
            void completionCallBack(SubTask sender);
            void exceptionCallBack(SubTask sender, Exception e);
        }

        public enum SUBTASK_SCHED_RESULT
        {
            FINISHED_OK,
            FINISHED_WITH_EXCEPTIONS,
            FINISHED_ON_STOP_SIGNAL
        }
        protected int numberTasksCompleted;
        protected const int POLLING_INTERVAL = 1;
        protected List<SubTask> tasks;
        protected bool isException;
        protected bool isAbortOnException=true;
        protected int maxNumberRunning=1;
        protected SubTask firstSubtaskWithException;

        protected ISubTaskSchedulerEnvironment schedEnv;
        protected Mutex stopMut;
        protected bool isStop;

        //protected void setRunning(bool v)
        //{
        //    runMut.WaitOne();
        //    isRunning = v;
        //    runMut.ReleaseMutex();
        //}

        //public bool IsRunning
        //{
        //    get
        //    {
        //        bool isR;
        //        runMut.WaitOne();
        //        isR = isRunning;
        //        runMut.ReleaseMutex();

        //        return isR;
        //    }
        //}

        public int NumberTasksCompleted
        {
            get { return numberTasksCompleted; }
        }

        public ISubTaskSchedulerEnvironment SchedEnv
        {
            get { return schedEnv; }
            set { schedEnv = value; }
        }

        public int MaxNumberRunningInParallel
        {
            get { return maxNumberRunning; }
            set { maxNumberRunning = value; }
        }

        public SubTaskScheduler()
        {
            tasks = new List<SubTask>();
            firstSubtaskWithException = null;
            mut = new Mutex();
            schedEnv = null;
            stopMut = new Mutex();
        }

        public void addSubTask(SubTask t)
        {
            tasks.Add(t);
        }

        public void stopExecution ()
        {
            stopMut.WaitOne();
            isStop =true;
            stopMut.ReleaseMutex ();
        }

        protected bool IsStop
        {
            get
            {
                bool iS;
                stopMut.WaitOne();
                iS = isStop;
                stopMut.ReleaseMutex();
                return iS;
            }
        }

        public SUBTASK_SCHED_RESULT execSubTaskSet()
        {
            int numberRunning ;
            bool isAllFinished,isStopped;

            numberTasksCompleted = 0;
            do
            {
                isAllFinished =true;
                numberRunning = 0;

                foreach (SubTask st in tasks)
                {
                    if (st.CurrentState !=SubTask .SubTaskState .FINISHED_OK )
                        isAllFinished =false;
                    if (st.CurrentState == SubTask.SubTaskState.RUNNING)
                        numberRunning++;
                }

                foreach (SubTask st in tasks)
                {
                    if (numberRunning < maxNumberRunning && 
                        st.CurrentState == SubTask.SubTaskState.NOT_STARTED && 
                        st.IsReadyToStart)
                    {
                        st.startSubTask();
                        numberRunning++;
                    }
                }
                Thread.Sleep(POLLING_INTERVAL);

                isStopped = IsStop;
            } while (!isAllFinished && !(isException && isAbortOnException) && !isStopped);

            if (isException||isStopped )
            {
                stopAllSubTasks();
                if (isException)
                    return SUBTASK_SCHED_RESULT.FINISHED_WITH_EXCEPTIONS;
                else
                    return SUBTASK_SCHED_RESULT.FINISHED_ON_STOP_SIGNAL;
            }
            else
                return SUBTASK_SCHED_RESULT.FINISHED_OK;
        }

        public void stopAllSubTasks()
        {
            foreach (SubTask st in tasks)
            {
                if (st.CurrentState == SubTask.SubTaskState.RUNNING)
                    st.stopSubTask();
            }           
        }

        public void completionCallBack(SubTask sender) 
        {
            sender.CurrentState = SubTask.SubTaskState.FINISHED_OK;

            if (schedEnv != null)
            {
                mut.WaitOne();
                numberTasksCompleted++;
                schedEnv.completionCallBack(sender);
                mut.ReleaseMutex();
            }
        }
        public void exceptionCallBack(SubTask sender,Exception e)
        {
            isException = true;
            sender.CurrentState = SubTask.SubTaskState.FINISHED_ERROR;
            sender.Exc = e;

            if (firstSubtaskWithException == null)
                firstSubtaskWithException = sender;
        }

        public SubTask FirstSubTaskWithException
        {
            get { return firstSubtaskWithException; }
        }
    }
}
